: # https://stackoverflow.com/questions/17510688/single-script-to-run-in-both-windows-batch-and-linux-bash
: # https://stackoverflow.com/questions/9513428/internal-ca-signed-cert-without-csr-certificate-sign-request
:<<: #
@echo off
goto :batch
:

if [ "$1" == "" ]; then #
    echo specify name #
    exit 1 #
fi #

openssl ecparam -name secp521r1 -genkey -noout -out $1.key #

exit 0 #

:batch
if "%~1"=="" (
    echo specify name
    exit /b 1
)

openssl ecparam -name secp521r1 -genkey -noout -out %1.key
