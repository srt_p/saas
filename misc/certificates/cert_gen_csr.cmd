: # https://stackoverflow.com/questions/17510688/single-script-to-run-in-both-windows-batch-and-linux-bash
: # https://stackoverflow.com/questions/9513428/internal-ca-signed-cert-without-csr-certificate-sign-request
:<<: #
@echo off
goto :batch
:

if [ "$1" == "" ]; then #
    echo specify name #
    exit 1 #
fi #

openssl req -new -config $1.cnf -key $1.key -out $1.csr #

exit 0 #

:batch
if "%~1"=="" (
    echo specify name
    exit /b 1
)

openssl req -new -config %1.cnf -key %1.key -out %1.csr