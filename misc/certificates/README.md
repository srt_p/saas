# Description
These are helper scripts for certificate stuff, they can be called e.g. with cmd (Windows)
```
<script> <arguments>
```
and bash
```
bash <script> <arguments>
```
The scripts take the following assumptions on filenames:
- configs: <name>.cnf
- certificates: <name>.crt
- certificate signing requests: <name>.csr
- private keys: <name>.key
- ca-folder has the same name as the ca

# Preparation
for each entity, create a config file, see the following examples
- sample ca config
  ```
  [ca]
  default_ca = ca_default

  [ca_default]
  dir = <name>
  serial = $dir/serial
  database = $dir/database
  new_certs_dir = $dir/new_certs
  certificate = <name>.crt
  private_key = <name>.key
  default_md = sha256
  default_days = 1100
  policy = ca_policy
  copy_extensions = copy

  [ca_policy]
  countryName = supplied
  stateOrProvinceName = supplied
  organizationName = supplied
  commonName = supplied

  [req]
  distinguished_name = req_distinguished_name
  prompt = no

  [req_distinguished_name]
  C = <country>
  ST = <state>
  O = <organizuation>
  CN = <name>

  [ext_ca]
  basicConstraints = critical, CA:TRUE
  subjectKeyIdentifier = hash
  authorityKeyIdentifier = keyid:always, issuer:always
  keyUsage = critical, cRLSign, digitalSignature, keyCertSign

  [ext_server]
  basicConstraints = critical, CA:FALSE
  subjectKeyIdentifier = hash
  authorityKeyIdentifier = keyid:always, issuer:always
  keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment, keyAgreement
  extendedKeyUsage = critical, serverAuth

  [ext_client]
  basicConstraints = critical, CA:FALSE
  subjectKeyIdentifier = hash
  authorityKeyIdentifier = keyid:always, issuer:always
  keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
  extendedKeyUsage = critical, clientAuth
  ```
- sample server config
  ```
  [req]
  distinguished_name = req_distinguished_name
  prompt = no
  req_extensions = req_ext

  [req_distinguished_name]
  C = <country>
  ST = <state>
  O = <organization>
  CN = <server name>

  [req_ext]
  subjectAltName=DNS:<dns-name>[,DNS:...]
  ```
- sample client config
  ```
  [req]
  distinguished_name = req_distinguished_name
  prompt = no

  [req_distinguished_name]
  C = <country>
  ST = <state>
  O = <organization>
  CN = <username>
  ```

# Usage
- generate a private key
  ```
  cert_gen_key.cmd <name>
  ```
  Hint: if you want to encrypt the private key use for example
  ```
  openssl ec -in <name>.key -out <name>_enc.key -aes256
  ```
  and delete the unencrypted key and rename the encrypted one
- generate a self signed certificate authority
  ```
  cert_gen_ca.cmd <name>
  ```
- generate a certificate signing request
  ```
  cert_gen_csr.cmd <name>
  ```
- sign a **server** certificate signing request
  ```
  cert_sign_server.cmd <name> <ca-name>
  ```
- sign a **client** certificate signing request
  ```
  cert_sign_client.cmd <name> <ca-name>
  ```
- revoke a signed certificate
  ```
  cert_revoke.cmd <name> <ca-name>
  ```
- print information about certificate signing request
  ```
  cert_print_csr.cmd <csr-file>
  ```
- print information about certificate
  ```
  cert_print.cmd <cert-file>
  ```

# Installation on client systems
- [Archlinux](https://unix.stackexchange.com/questions/373492/installing-certificates-on-arch)
- [Windows]()
- Hint: for firefox set security.enterprise_roots.enabled to true

# Sources
- https://www.scottbrady91.com/OpenSSL/Creating-Elliptical-Curve-Keys-using-OpenSSL
- https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/
- https://www.ssls.com/knowledgebase/how-to-import-intermediate-and-root-certificates-via-mmc/
- https://tarunlalwani.com/post/self-signed-certificates-trusting-them/
- https://gist.github.com/Soarez/9688998
- https://man.openbsd.org/x509v3.cnf.5
- https://superuser.com/questions/738612/openssl-ca-keyusage-extension
