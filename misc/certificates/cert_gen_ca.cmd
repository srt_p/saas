: # https://stackoverflow.com/questions/17510688/single-script-to-run-in-both-windows-batch-and-linux-bash
: # https://stackoverflow.com/questions/9513428/internal-ca-signed-cert-without-csr-certificate-sign-request
:<<: #
@echo off
goto :batch
:

if [ "$1" == "" ]; then #
    echo specify name #
    exit 1 #
fi #

openssl req -new -config $1.cnf -key $1.key -out $1.csr #
mkdir -p $1/new_certs #
> $1/serial #
> $1/database #
echo unique_subject = yes > $1/database.attr
openssl ca -selfsign -create_serial -rand_serial -config $1.cnf -in $1.csr -out $1.crt -extensions ext_ca #

exit 0 #

:batch
if "%~1"=="" (
    echo specify name
    exit /b 1
)

openssl req -new -config %1.cnf -key %1.key -out %1.csr
if not exist %1 mkdir %1
if not exist %1\new_certs mkdir %1\new_certs
<nul (set/p z=) >%1\serial
<nul (set/p z=) >%1\database
echo unique_subject = yes >%1\database.attr
openssl ca -selfsign -create_serial -rand_serial -config %1.cnf -in %1.csr -out %1.crt -extensions ext_ca
