:<<: #
@echo off
goto :batch
:

if [ "$1" == "" ]; then #
    echo specify cert file #
    exit 1 #
fi #

openssl x509 -noout -text -in "$1" #

exit 0 #

:batch
if "%~1"=="" (
    echo specify cert file
    exit /b 1
)

openssl x509 -noout -text -in "%1"
