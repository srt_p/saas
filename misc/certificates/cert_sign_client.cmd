: # https://stackoverflow.com/questions/17510688/single-script-to-run-in-both-windows-batch-and-linux-bash
: # https://stackoverflow.com/questions/9513428/internal-ca-signed-cert-without-csr-certificate-sign-request
:<<: #
@echo off
goto :batch
:

if [ "$1" == "" ] || [ "$2" == "" ]; then #
    echo specify name and ca name #
    exit 1 #
fi #

openssl ca -config $2.cnf -rand_serial -in $1.csr -out $1.crt -extensions ext_client #
exit 0 #

:batch
if "%~1"=="" goto err
if "%~2"=="" goto err
goto noerr

:err
echo specify name and ca name
exit /b 1

:noerr
openssl ca -config %2.cnf -rand_serial -in %1.csr -out %1.crt -extensions ext_client