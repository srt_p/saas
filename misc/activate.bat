@echo off
setlocal enabledelayedexpansion

set "ip=x.x.x.x"

choice /C YN /M "Windows aktivieren?"
if errorlevel 2 goto end_windows
if errorlevel 1 goto activate_windows

:activate_windows
cscript /NoLogo C:\Windows\System32\slmgr.vbs -upk
cscript /NoLogo C:\Windows\System32\slmgr.vbs -ipk W269N-WFGWX-YVC9B-4J6C9-T83GX
cscript /NoLogo C:\Windows\System32\slmgr.vbs -skms %ip%
cscript /NoLogo C:\Windows\System32\slmgr.vbs -ato

:end_windows

choice /C YN /M "Office aktivieren?"
if errorlevel 2 goto end_office
if errorlevel 1 goto activate_office

:activate_office
cscript /NoLogo "C:\Program Files\Microsoft Office\Office16\ospp.vbs" /sethst:%ip%

reg query "HKLM\SOFTWARE\Microsoft\Office\ClickToRun\Configuration" /v VersionToReport >nul 2>&1
if %errorlevel% neq 0 (
    echo Microsoft Office is not installed or not detected
    exit /b
)

for /f "tokens=3" %%a in ('reg query "HKLM\SOFTWARE\Microsoft\Office\ClickToRun\Configuration" /v VersionToReport ^| find "VersionToReport"') do set "version=%%a"

for /f "tokens=1,3 delims=." %%a in ("!version!") do (
    set "majorVersion=%%a"
    set "buildNumber=%%b"
)

if !majorVersion! equ 16 (
    if !buildNumber! geq 14000 (
        if !buildNumber! geq 16000 (
            rem Office 2024
            set "officekey=XJ2XN-FW8RK-P4HMP-DKDBV-GCVGB"
        ) else (
            if !buildNumber! geq 15000 (
                rem Office 2021
                set "officekey=FXYTK-NJJ8C-GB6DW-3DYQT-6F7TH"
            ) else (
                rem Office 2019
                set "officekey=NMMKJ-6RK4F-KMJVX-8D9MJ-6MWKP"
            )
        )
    ) else (
        rem Office 2016
    )
) else (
    if !majorVersion! equ 15 (
        rem Office 2013
    )
    if !majorVersion! equ 14 (
        rem Office 2010
    )
)

cscript /NoLogo "C:\Program Files\Microsoft Office\Office16\ospp.vbs" /inpkey:%officekey%
cscript /NoLogo "C:\Program Files\Microsoft Office\Office16\ospp.vbs" /act

:end_office
