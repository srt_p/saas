# Information
this is a description for a vlmcsd server in a virtual machine without the need for seperate server hardware.

# Preparing the Virtual Machine
1. Create Virtual Machine
   - VirtualBox<br>
     Install VirtualBox for Windows and some Linux Distro on it (e.g. AlpineLinux-virt) in a vm named e.g. 'vlmcsd'.<br>
     Don't reboot but poweroff the machine and remove the booted iso file. Set the network adapter type to 'Bridged Adapter'.
   - Qemu<br>
     Install Qemu for Windows and some Linux Distro on it, see below (e.g. AlpineLinux-virt). Install TAP-driver from openvpn-Project (Community) and configure it with IP 172.16.0.1 and netmask 255.240.0.0, gateway is not needed. Set the name of the TAP-Adapter to 'TAP'
     ```
     \Path\to\qemu\qemu-img create vlmcsd.img 512M
     \Path\to\qemu\qemu-system-i386 -boot d -cdrom \Path\to\iso -drive file=vlmcsd.img,index=0,media=disk,format=raw -m 256
     ```
2. Install basic tools
   - VirtualBox<br>
     uncomment 'community'-line in /etc/apk/repositories
     ```
     apk update
     apk upgrade
     apk add git make build-base virtualbox-guest-additions
     ```
   - Qemu<br>
     boot the iso e.g. with
     ```
     \Path\to\qemu\qemu-system-i386 -drive file=vlmcsd.img,index=0,media=disk,format=raw -m 256
     ```
     then
     ```
     apk update
     apk upgrade
     apk add git make build-base
     ```
3. Install vlmcsd
   ```
   git clone --branch master --single-branch https://github.com/Wind4/vlmcsd.git
   cd vlmcsd
   make
   cd ..
   cp vlmcsd/bin/vlmcsd /usr/bin/vlmcsd
   rm -rf vlmcsd
   ```
4. create and enable the vlmcsd-service
   ```
   touch /etc/init.d/vlmcsd
   chmod 755 /etc/init.d/vlmcsd
   ```
   fill the file with the following information
   ```
   #!/sbin/openrc-run
   description="start vlmcsd"
   command="/usr/bin/vlmcsd"
   command_args="-d -D"
   command_background=true
   pidfile="/run/${RC_SVCNAME}.pid"
   ```
5. activate the service and stop the vm
   - VirtualBox
     ```
     rc-update add vlmcsd
     rc-update add virtualbox-guest-additions
     poweroff
     ```
   - Qemu<br>
     some additional configuration is needed here, fill /etc/network/interfaces with the following information:
     ```
     auto lo
     iface lo inet loopback

     auto eth0
     iface eth0 inet static
         address 172.16.0.2
         netmask 255.240.0.0

     auto eth1
     iface eth1 inet dhcp
     ```
     then execute
     ```
     rc-update add vlmcsd
     poweroff
     ```

# Prepare the Host
Copy all scripts from this directory to some directory on the host. Eventually unblock the file from 'Windows Defender SmartScreen' (Preferences -> General -> Unblock)

# Run
Set the *bUseQemu* variable in *vlmcsd_activate.ps1* to *$TRUE* or *$FALSE* respectively. If necessary, adjust the Path variables too. Then just execute the *run.bat* script, it should ask for permissions automatically and after all Windows have closed the software should be activated

# Sources
- http://wiki.laptop.org/go/Talk:Using_QEMU_on_Windows
- https://gist.github.com/extremecoders-re/e8fd8a67a515fee0c873dcafc81d811c
