@echo off

set dir=%~dp0

set args=%1
shift
:start
if [%1] == [] goto done
set args=%args% %1
shift
goto start
:done

start /d %dir% /b /wait powershell.exe -noLogo -ExecutionPolicy bypass -File %dir%\vlmcsd_firewall.ps1 %args%

exit /b 0
