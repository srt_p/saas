# the '?' are delimiters
$iCtr = 0
while($iCtr -lt $args.Count) {
    $sVBoxPath = $sVBoxPath + " " + $args[$iCtr]
    ++$iCtr
    if($sVBoxPath.Substring($sVBoxPath.Length - 1) -eq '?') {break}
}
while($iCtr -lt $args.Count) {
    $sQemuPath = $sQemuPath + " " + $args[$iCtr]
    ++$iCtr
    if($sQemuPath.Substring($sQemuPath.Length - 1) -eq '?') {break}
}
$sVBoxPath = $sVBoxPath.Substring(2, $sVBoxPath.Length - 3)
$sQemuPath = $sQemuPath.Substring(2, $sQemuPath.Length - 3)

try {
    Get-NetFirewallRule -ErrorAction Stop -Name "VirtualBox VM TCP"
} catch {
    if(Test-Path "$sVBoxPath\virtualboxvm.exe") {
        New-NetFirewallRule -Program "$sVBoxPath\virtualboxvm.exe" -DisplayName "VirtualBox VM TCP" -Name "VirtualBox VM TCP" -Protocol TCP
    }
}
try {
    Get-NetFirewallRule -ErrorAction Stop -Name "VirtualBox VM UDP"
} catch {
    if(Test-Path "$sVBoxPath\virtualboxvm.exe") {
        New-NetFirewallRule -Program "$sVBoxPath\virtualboxvm.exe" -DisplayName "VirtualBox VM UDP" -Name "VirtualBox VM UDP" -Protocol UDP
    }
}
try {
    Get-NetFirewallRule -ErrorAction Stop -Name "QEMU VM TCP"
} catch {
    if(Test-Path "$sQemuPath\qemu-system-i386.exe") {
        New-NetFirewallRule -Program "$sQemuPath\qemu-system-i386.exe" -DisplayName "QEMU VM TCP" -Name "QEMU VM TCP" -Protocol TCP
    }
}
try {
    Get-NetFirewallRule -ErrorAction Stop -Name "QEMU VM UDP"
} catch {
    if(Test-Path "$sQemuPath\qemu-system-i386.exe") {
        New-NetFirewallRule -Program "$sQemuPath\qemu-system-i386.exe" -DisplayName "QEMU VM UDP" -Name "QEMU VM UDP" -Protocol UDP
    }
}
