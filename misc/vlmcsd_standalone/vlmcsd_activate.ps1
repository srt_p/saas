$sFirewallScript = "vlmcsd_firewall.bat"
$sActScript = "vlmcsd_act.bat"
$bUseQemu = $TRUE
$sVBoxPath = "C:\Program Files\Oracle\VirtualBox"
$sQemuPath = "C:\Program Files\qemu"
$sQemuGuestIP = "172.16.0.2"
if(-Not ($args.Count -eq 1)) {
    Write-Output "no argument received, first (and only) argument should be the path to the scripts folder"
    exit
} else {
    $sPath = $args[0]
}

function execute_as_admin($sFile, [String[]]$aArguments) {
    if(-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
        if([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
            Start-Process -Wait -FilePath "$sFile" -Verb Runas -ArgumentList $aArguments
        } else {
    	    Write-Output "cannot start as admin, windows build to early"
    		exit
    	}
    } else {
        Start-Process -NoNewWindow -Wait -FilePath "$sFile" -ArgumentList $aArguments
    }
}

execute_as_admin "$sPath\$sFirewallScript" @("?${sVBoxPath}?","?${sQemuPath}?")

if($bUseQemu) {
    Start-Process -FilePath "$sQemuPath\qemu-system-i386.exe" -ArgumentList `
        "-drive","file=vlmcsd.img,index=0,media=disk,format=raw","-m","256",`
        "-netdev","tap,ifname=TAP,id=tap0","-device","e1000,netdev=tap0",`
        "-netdev","user,id=user0","-device","e1000,netdev=user0"
    while(-Not (Test-NetConnection -ComputerName $sQemuGuestIP -Port 1688 -WarningAction Silently).tcpTestSucceeded) {}
    execute_as_admin "$sPath\$sActScript" @($sQemuGuestIP)
    Get-Process | Where-Object {$_.Path -like "$sQemuPath\qemu-system-i386.exe"} | Stop-Process -Force
} else {
    Start-Process -NoNewWindow -FilePath "$sVBoxPath\VBoxManage.exe" -Wait -ArgumentList "startvm","vlmcsd"
    Start-Sleep -Seconds 5
    Start-Process -NoNewWindow -FilePath "$sVBoxPath\VBoxManage.exe" -Wait -ArgumentList "guestproperty","wait","vlmcsd","/VirtualBox/GuestInfo/OS/LoggedInUsers"
    $sVBoxIP = $(& "$sVBoxPath\VBoxManage.exe" @("guestproperty","get","vlmcsd","/VirtualBox/GuestInfo/Net/0/V4/IP"))
    $sVBoxIP = $sVBoxIP.split(" ")[1]
    execute_as_admin "$sPath\$sActScript" @($sVBoxIP)
    Start-Process -NoNewWindow -FilePath "$sVBoxPath\VBoxManage.exe" -Wait -ArgumentList "controlvm","vlmcsd","acpipowerbutton"
}
