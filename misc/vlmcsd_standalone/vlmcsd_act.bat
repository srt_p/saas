@echo off

cscript /NoLogo C:\Windows\System32\slmgr.vbs -upk
cscript /NoLogo C:\Windows\System32\slmgr.vbs -ipk W269N-WFGWX-YVC9B-4J6C9-T83GX
cscript /NoLogo C:\Windows\System32\slmgr.vbs -skms %1
cscript /NoLogo C:\Windows\System32\slmgr.vbs -ato

if exist "C:\Program Files\Microsoft Office\Office16\ospp.vbs" (
    cscript /NoLogo "C:\Program Files\Microsoft Office\Office16\ospp.vbs" /sethst:%1
    cscript /NoLogo "C:\Program Files\Microsoft Office\Office16\ospp.vbs" /inpkey:NMMKJ-6RK4F-KMJVX-8D9MJ-6MWKP
    cscript /NoLogo "C:\Program Files\Microsoft Office\Office16\ospp.vbs" /act
)

if exist "C:\Program Files (x86)\Microsoft Office\Office16\ospp.vbs" (
    cscript /NoLogo "C:\Program Files (x86)\Microsoft Office\Office16\ospp.vbs" /sethst:%1
    cscript /NoLogo "C:\Program Files (x86)\Microsoft Office\Office16\ospp.vbs" /inpkey:NMMKJ-6RK4F-KMJVX-8D9MJ-6MWKP
    cscript /NoLogo "C:\Program Files (x86)\Microsoft Office\Office16\ospp.vbs" /act
)

exit /b 0
