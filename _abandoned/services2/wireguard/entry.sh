#!/bin/sh

if [ ! -f /wireguard/privatekey ]; then
    wg genkey | tee /wireguard/privatekey | wg pubkey > /wireguard/publickey
fi
if [ ! -f /wireguard/publickey ]; then
    cat /wireguard/privatekey | wg pubkey > /wireguard/publickey
fi

cat << EOF > /etc/wireguard/wg0.conf
[Interface]
Address = ${ADDRESS}
ListenPort = 51820
PrivateKey = $(cat /wireguard/privatekey)
PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
EOF

mkdir -p /wireguard/peers
for peer in /wireguard/peers/*; do
    if [ -f "$peer" ]; then
        echo "[Peer]" >> /etc/wireguard/wg0.conf
        cat "$peer" >> /etc/wireguard/wg0.conf
    fi
done
chmod 400 /etc/wireguard/wg0.conf

exec $@
