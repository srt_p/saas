# Description
Dockerized wireguard server designed for external devices connecting to internal LAN. This description assumes VPN IPs in the 10.10.0.0/16 network, feel free to adjust.

# Installation
- move this wireguard-service folder to e.g. `/docker/wireguard`
- adjust `docker-compose.yml` and other configuration if necessary
- server keys are created on the first start of the container in the volumes folder
- start the server once, so docker takes care of restarting
  ```
  cd /docker/wireguard
  docker-compose up -d
  ```
# Configuration
- add a client
  - server side
    - add a file to `volumes/peers` containing the client configuration, e.g.
      ```
      PublicKey = <client public key>
      AllowedIPs = 10.10.x.x/32
      ```
    - restart the container
  - client side
    - add a configuration like this
      ```
      [Interface]
      Address = 10.10.x.x/32
      PrivateKey = <client private key>
      DNS = <DNS server>

      [Peer]
      PublicKey = <server public key>
      Endpoint = <public ip>:<port>
      AllowedIPs = 10.10.0.0/16,<LAN net, e.g. 192.168.0.0/16>
      ```

# Sources
- https://www.stavros.io/posts/how-to-configure-wireguard/
