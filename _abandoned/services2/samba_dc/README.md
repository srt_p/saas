# Description
This is a Samba Active Directory Domain Controller service.

# Installation
- move this samba_dc-service folder to e.g. `/docker/samba_dc`
- adjust the configuration if needed
- the service uses host network, but the interface samba runs on can be specified (e.g. by dns name or ip)
- if `volumes/samba` is empty or not existent, the domain is automatically created, afterwards the environment variables set in docker-compose.yml have no effect
- copy your *.crt* and *.key* files to `volumes` (see `docker-compose.yml`)
- start the server once, so docker takes care of restarting
  ```
  cd /docker/samba_dc
  docker-compose up -d
  ```
- **set the root passwords**
  [TODO]

# Usage
- user management
  - list users
    ```
    samba-tool user list | sort
    ```
  - add user
    ```
    samba-tool user create <user>
    ```
  - add user to group
    ```
    samba-tool group addmembers "<group>" <user>
    ```

# Sources
- https://github.com/LasLabs/docker-alpine-samba-dc
- https://www.tecmint.com/install-samba4-active-directory-ubuntu/
- https://wiki.archlinux.org/index.php/Samba/Active_Directory_domain_controller
- https://wiki.samba.org/index.php/Active_Directory_Naming_FAQ
- https://wiki.samba.org/index.php/Setting_up_Samba_as_an_Active_Directory_Domain_Controller
- https://stackoverflow.com/questions/54078590/querying-samba-ad-server-with-ldapsearch-fails-with-ldap-sasl-bindsimple-can
- https://lists.samba.org/archive/samba/2016-April/199234.html
- https://www.tecmint.com/install-samba4-active-directory-ubuntu/
