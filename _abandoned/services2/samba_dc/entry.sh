#!/bin/bash

cp -f /ca.crt /etc/pki/ca-trust/source/anchors/
update-ca-trust

# prepare certificates
cp -f /samba.crt /etc/samba/crt.pem
cp -f /samba.key /etc/samba/key.pem
#openssl x509 -inform DER -outform PEM -in /samba.crt -out /etc/samba/crt.pem
#openssl rsa -inform DER -outform PEM -in /samba.key -out /etc/samba/key.pem
chmod 600 /etc/samba/key.pem

echo "[libdefaults]" > /etc/krb5.conf
echo "default_realm = ${REALM}" >> /etc/krb5.conf
echo "dns_lookup_realm = false" >> /etc/krb5.conf
echo "dns_lookup_kdc = true" >> /etc/krb5.conf

rm -f /etc/samba/smb.conf
if [ -f /etc/samba/external/smb.conf ]; then
    cp -f /etc/samba/external/smb.conf /etc/samba/smb.conf
else
    samba-tool domain provision \
        --domain=${DOMAIN} \
        --realm=${REALM} \
        --server-role=dc \
        --adminpass=${ADMINPASS} \
        --krbtgtpass=${KRBTGTPASS} \
        --machinepass=${MACHINEPASS} \
        --dnspass=${DNSPASS} \
        --host-ip=${HOSTIP} \
        --option="template shell = /bin/bash" \
        --option="winbind use default domain = true" \
        --option="winbind offline logon = false" \
        --option="bind interfaces only = yes" \
        --option="interfaces = ${INTERFACE}" \
        --option="tls enabled = yes" \
        --option="tls keyfile = /etc/samba/key.pem" \
        --option="tls certfile = /etc/samba/crt.pem" \
        --option="tls cafile =" \
        --option="smb encrypt = required"

    # disable password complexity
    samba-tool domain passwordsettings set --complexity=off
    samba-tool domain passwordsettings set --history-length=0
    samba-tool domain passwordsettings set --min-pwd-age=0
    samba-tool domain passwordsettings set --max-pwd-age=0

    cp /etc/samba/smb.conf /etc/samba/external/smb.conf
fi

exec $@
