# Description
Dockerized pyload server.

# Installation
- move this pyload-service folder to e.g. `/docker/pyload`
- adjust `docker-compose.yml`
- copy pyload.key and pyload.crt to volumes
- adjust other configuration if necessary
- start the server once, so docker takes care of restarting
  ```
  cd /docker/pyload
  docker-compose up -d
  ```

# Tipps
- forward click'n'load to other host, with `cmd` as admin:
  ```
  netsh interface portproxy add v4tov4 listenaddress=127.0.0.1 listenport=9666 connectaddress=<host> connectport=9666
  ```
  to remove:
  ```
  netsh interface portproxy delete v4tov4 listenaddress=127.0.0.1 listenport=9666 
  ```
- by default the downloads placed in `volumes/downloads`, you can for example mount a hard drive there, uuid can be found with `blkid`
  ```
  mkdir -p volumes/downloads
  echo "UUID=<uuid> /path/to/volumes/downloads ext4 rw,relatime 0 0" >> /etc/fstab
  reboot
  ```

# Sources
- https://github.com/pyload/pyload
- https://github.com/pyload/pyload/blob/stable/module/database/UserDatabase.py
- https://github.com/linuxserver/docker-pyload/blob/master/root/etc/cont-init.d/30-config
