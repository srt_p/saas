#!/bin/bash

PYTHON_PW_HASH=$(cat <<EOF
import os, random
from hashlib import sha1
salt = reduce(lambda x, y: x + y, [str(random.randint(0, 9)) for i in range(0, 5)])
h = sha1(salt + os.getenv('PYLOAD_PASS'))
print(salt + h.hexdigest())
EOF
)

if [ ! -f "/config/files.db" ]; then
    echo "Initial DB setup, this may take some time"
    # start /cmd.sh and output to fd 3
    exec 3< <(/cmd.sh)
    while read line; do
        if [[ "$line" == *"pyLoad is up and running"* ]]; then break
        else echo "$line"; fi
    done <&3
    kill $(cat /config/pyload.pid)
    # close fd 3
    exec 3<&-
    if [ -f "/config/files.db" ]; then
        pw_hash=$(python -c "${PYTHON_PW_HASH}")
        /usr/bin/sqlite3 /config/files.db "\
            INSERT INTO \
                users(\
                    'name',\
                    'password') \
                VALUES(\
                    '$PYLOAD_USER',\
                    '$pw_hash');"
    else
        echo "ERROR: something went wrong with DB setup, you won't be able to login"
    fi
fi

exec $@
