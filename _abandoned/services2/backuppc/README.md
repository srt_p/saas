# Description
Dockerized BackupPC Server

# Todo
- email
- micluster

# Installation
- move this backuppc-service folder to e.g. `/docker/backuppc`
- adjust `docker-compose.yml`
- if you don't have a config file already start the server once and stop it again so the default config file will be created, then adjust the `CgiAdminUserGroup` and/or `CgiAdminUser` settings to enable admin access (default is admin access for all valid users)
- start the server once again, so docker takes care of restarting
  <pre>
  cd /docker/backuppc
  docker-compose up -d
  </pre>

# Configuration
- authentication can be configured in the `lighttpd` volume in `lighttpd.conf`. Bear in mind that most of the lighttpd configuration is already done (see `entry.sh`)
  - example for basic auth
    <pre>
    config/lighttpd.conf<hr>auth.backend = "plain"
    auth.backend.plain.userfile = "/etc/lighttpd/passwd"
    </pre>
    user config
    <pre>
    passwd<hr>user1:password1
    user2:password2
    ...
    </pre>
    docker-compose
    <pre>
    ...
    volumes:
      - /path/to/volumes/passwd:/etc/lighttpd/passwd
    ...
    </pre>
  - example for ldap auth
    <pre>
    config/lighttpd.conf<hr>server.modules += ( "mod_authn_ldap" )
    auth.backend = "ldap"
    auth.backend.ldap.hostname = "ldap.example.com"
    auth.backend.ldap.filter = "(cn=$)"
    auth.backend.ldap.starttls = "enable"
    auth.backend.ldap.base-dn = "dc=example,dc=com"
    auth.backend.ldap.ca-file = "/ca.crt"
    auth.backend.ldap.bind-dn = "cn=lighttpd,cn=Users,dc=example,dc=com"
    auth.backend.ldap.bind-pw = "secret"
    auth.backend.ldap.allow-empty-pw = "disable"
    </pre>
    docker-compose
    <pre>
    ...
    volumes:
      - /path/to/volumes/ca.crt:/ca.crt
    ...
    </pre>
    **Notes:**
    - ldap groups don't seem to work in backuppc configuration, you can set `$Conf{CgiAdminUsers} = '*';` and adjust the ldap filter to something like `(&(cn=$)(memberOf=cn=Domain Admins,cn=Users,dc=ad,dc=srt))`

# Sources
