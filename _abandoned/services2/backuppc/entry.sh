[[ -d /backuppc/conf ]] || cp -r /etc/BackupPC_orig /backuppc/conf
[[ -d /backuppc/top ]] || cp -r /var/lib/BackupPC_orig /backuppc/top
[[ -d /backuppc/log ]] || mkdir -p /backuppc/log
[[ -f /lighttpd/log.error ]] || touch /lighttpd/log.error

[[ "x$(stat --format '%U' /backuppc/conf)" = "backuppc" ]] ||
    chown -R backuppc:backuppc /backuppc/conf
[[ "x$(stat --format '%U' /backuppc/top)" = "backuppc" ]] ||
    chown -R backuppc:backuppc /backuppc/top
[[ "x$(stat --format '%U' /backuppc/log)" = "backuppc" ]] ||
    chown -R backuppc:backuppc /backuppc/log
chown -R backuppc:backuppc /lighttpd

cat > /etc/lighttpd/lighttpd.conf << EOF
server.port = 80
server.username = "backuppc"
server.groupname = "backuppc"
server.document-root = "/var/www/html"
server.errorlog = "/lighttpd/log.error"
dir-listing.activate = "enable"
index-file.names = ( "index.html", "index.php", "index.cgi" )
mimetype.assign = (
    ".html" => "text/html",
    ".txt" => "text/plain",
    ".jpg" => "image/jpeg",
    ".png" => "image/png",
    "" => "application/octet-stream"
)
server.modules = (
    "mod_alias",
    "mod_cgi",
    "mod_auth",
    "mod_access",
    "mod_openssl",
    "mod_redirect"
)
alias.url = ( "/BackupPC_Admin" => "/usr/share/BackupPC/sbin/BackupPC_Admin" )
alias.url += ( "/BackupPC/images" => "/usr/share/BackupPC/html" )
cgi.assign += ( "BackupPC_Admin" => "/usr/bin/perl" )
include "/lighttpd/lighttpd.conf"
auth.require = ( "/BackupPC_Admin" => (
    "method" => "basic",
    "realm" => "BackupPC",
    "require" => "valid-user"
))
\$SERVER["socket"] == ":80" {
    url.redirect = ( "" => "https://\${url.authority}\${url.path}\${qsa}" )
}
\$SERVER["socket"] == ":443" {
    ssl.engine = "enable"
    ssl.pemfile = "/backuppc.crt"
    ssl.privkey = "/backuppc.key"
}
EOF

exec $@
