# Description
Dockerized WebDAV Server for KeePass database hosting. Practically it is a simple WebDAV Server that creates personal spaces for defined users from LDAP and allows access to the directory itself, the <user>.kdbx and <user>.kdbx.tmp files.

# Installation
- move this keepass-service folder to e.g. `/docker/keepass`
- adjust `docker-compose.yml`
- [TODO] ssl
- start the server once
  <pre>
  cd /docker/keepass
  docker-compose up -d
  </pre>
