#!/bin/ash

for user in $(cat /webdav.pass | grep -o '^[^:]*'); do
    if [ -n "$user" ]; then
        mkdir -p /webdav/$user
        chown -R nginx:nginx /webdav/$user
    fi
done
