<?php
# General
$lang = "de";
$show_menu = false;
$show_help = false;
$background_image = "images/custom/back.png";
$logo = "";
$use_question = false;
$display_footer = false;

# Security
$keyphrase = "{{SSP_KEYPHRASE}}";
$login_forbidden_chars = ""; # only allow alphanumeric characters
$obscure_failure_messages = array("mailnomatch");
$who_change_password = "user";

# Default action
$default_action = "change";
$use_change = true;

# LDAP
$ldap_url = "{{SSP_LDAP_URL}}";
$ldap_starttls = false;
$ldap_binddn = "{{SSP_LDAP_BIND_DN}}";
$ldap_bindpw = "{{SSP_LDAP_BIND_PW}}";
$ldap_base = "{{SSP_LDAP_BASE}}";
$ldap_login_attribute = "cn";
$ldap_fullname_attribute = "cn";
$ldap_filter = "(&(objectClass=user)(sAMAccountName={login}))";
$ldap_use_exop_passwd = false;

# Active Directory mode
$ad_mode = true;
$ad_options['force_unlock'] = false;
$ad_options['force_pwd_change'] = false;
$ad_options['change_expired_password'] = true;

# Show extended error message returned by LDAP directory when password is refused
$show_extended_error = true;

# Extra messages
$messages['passwordchangedextramessage'] = NULL;
$messages['changehelpextramessage'] = NULL;
