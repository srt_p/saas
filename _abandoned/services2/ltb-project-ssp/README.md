# Description
Self Service Password is a PHP application that allows users to change their password in an LDAP directory.

# Installation
- move this ltb-ssp-service folder to e.g. `/docker/ltb-project-ssp`
- adjust *<ip>* and other stuff in `docker-compose.yml`
- adjust the rest of the configuration if needed
- the configured bind dn has to have read rights on the users
- copy your *.crt* and *.key* files to `volumes`
- start the server once, so docker takes care of restarting
  ```
  cd /docker/ltb-project-ssp
  docker-compose up -d
  ```

# Sources
- https://ltb-project.org/documentation/self-service-password/latest/start
- https://www.php.net/manual/fr/function.ldap-exop-passwd.php
- https://github.com/ltb-project/self-service-password
