#!/bin/sh

PHP_VERSION=$(php -v | sed -n 's/PHP \([0-9]\.[0-9]\).*/\1/p')

cp /ca.crt /usr/local/share/ca-certificates
update-ca-certificates

sed -e "s|{{NGINX_HOST}}|$NGINX_HOST|g" \
    -e "s|{{SSP_PHP_VERSION}}|$PHP_VERSION|g" \
    /nginx.default.template > /etc/nginx/sites-enabled/default

sed -e "s|{{SSP_KEYPHRASE}}|$(openssl rand -base64 32)|g" \
    -e "s|{{SSP_LDAP_URL}}|$SSP_LDAP_URL|g" \
    -e "s|{{SSP_LDAP_BASE}}|$SSP_LDAP_BASE|g" \
    -e "s|{{SSP_LDAP_BIND_DN}}|$SSP_LDAP_BIND_DN|g" \
    -e "s|{{SSP_LDAP_BIND_PW}}|$SSP_LDAP_BIND_PW|g" \
    /ssp/conf/config.inc.local.php.template > /ssp/conf/config.inc.local.php

php-fpm$PHP_VERSION -D
nginx -g "daemon off;"
