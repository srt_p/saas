# Installation
create necessary directories
```
mkdir -p /docker/psono-client
mkdir -p /docker-volumes/psono-client
```








copy necessary files, don't copy config files if you already have them
```
cp docker-compose.yml /docker/psono-server/
cp Dockerfile /docker/psono-server/
cp settings.yaml /docker-volumes/psono-server/
```
adjust the config files to fit your needs, if settings.yaml is "fresh" without the appropriate keys they can be generated with. Preplace the values in settings.yaml.
```
cd /docker/psono-server
docker-compose run --rm psono-server python3 ./psono/manage.py generateserverkeys
```

last, start the server once, so docker takes care of restarting
```
cd /docker/psono-server
docker-compose run --rm psono-server python3 ./psono/manage.py migrate
docker-compose up -d
```

# Tips
send a test email
```
docker-compose run --rm psono-server python3 ./psono/manage.py sendtestmail <email>
```

# Sources
- https://doc.psono.com/mydoc_index.html
- https://gitlab.com/psono/psono-server
- https://github.com/mcuadros/ofelia
- https://stackoverflow.com/questions/24718706/backup-restore-a-dockerized-postgresql-database
