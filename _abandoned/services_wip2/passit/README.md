# Installation
- move this passit-service folder to e.g. `/docker/passit`
- [2020-02-25] some additional steps are necessary, so
  - clone the *passit-backend*
    ```
    git clone https://gitlab.com/passit/passit-backend.git
    ```
  - add *libffi-dev* and *libpq-dev* to the `apt-get install` list in the Dockerfile
- adjust *<ip>* and other stuff in `docker-compose.yml`
- adjust the rest of the configuration if needed
- **limit access to `passit.env` as it contains secrets**
  ```
  chmod 400 /path/to/passit.env
  ```
- copy your *.crt* and *.key* files to volumes
- start the server once, so docker takes care of restarting
  ```
  cd /docker/passit
  docker-compose build
  docker-compose run --rm passit_web ./manage.py migrate
  docker-compose up -d

  cd /docker/passit
  docker-compose up -d
  docker-compose run --rm web ./manage.py migrate
  ```

# Sources
- https://disablessl3.com
- https://passit.io/install/
- https://stackoverflow.com/questions/24718706/backup-restore-a-dockerized-postgresql-database
- https://github.com/Koff/alpine-python3-django-uwsgi-nginx
