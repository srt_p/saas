#!/bin/sh

mkdir -p /usr/local/apache2/var
chown -R daemon:daemon /usr/local/apache2/var
chown -R daemon:daemon /webdav

httpd-foreground
