<?php

$config->custom->session['blowfish'] = "{{PLA_CONF_SESSION_BLOWFISH}}";

/*********************************************
 * Logging                                   *
 *********************************************/
$config->custom->debug['level'] = 16;
$config->custom->debug['syslog'] = true;
$config->custom->debug['file'] = '/log/pla_debug.log';

/*********************************************
 * Appearance                                *
 *********************************************/
$config->custom->appearance['hide_template_warning'] = true;

/*********************************************
 * Define your LDAP servers in this section  *
 *********************************************/
$servers = new Datastore();

$servers->newServer('ldap_pla');
$servers->setValue('server','name','config');
$servers->setValue('server','host','ldaps://<host>:636');
$servers->setValue('server','port',0);
$servers->setValue('server','base',array('cn=config'));
$servers->setValue('appearance','open_tree',true);

$servers->newServer('ldap_pla');
$servers->setValue('server','name','data');
$servers->setValue('server','host','ldaps://<host>:636');
$servers->setValue('server','port',0);
$servers->setValue('appearance','open_tree',true);
?>
