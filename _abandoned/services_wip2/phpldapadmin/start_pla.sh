#!/bin/sh

rm -rf /code/*
cp -r /phpldapadmin /code
cp -f /config.php /code/phpldapadmin/config/config.php

chown -R www-data:www-data /log
cp /kgd.crt /usr/local/share/ca-certificates
update-ca-certificates

# set random value for cookie encryption
/bin/sed -i -e "s|{{PLA_CONF_SESSION_BLOWFISH}}|$(/usr/bin/openssl rand -base64 32)|g" /code/phpldapadmin/config/config.php

/usr/sbin/php-fpm$(php -v | sed -n 's/PHP \([0-9]\.[0-9]\).*/\1/p') -F
