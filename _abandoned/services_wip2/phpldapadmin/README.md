# Installation
- move this phpldapadmin-service folder to e.g. `/docker/phpldapadmin`
- adjust *<ip>* and other stuff in `docker-compose.yml`
- adjust the rest of the configuration if needed, especially *<host>* in config.php
- copy your *.crt* and *.key* files to `volumes`
- start the server once, so docker takes care of restarting
  ```
  cd /docker/wolweb
  docker-compose up -d
  ```

# Sources
- http://phpldapadmin.sourceforge.net/wiki/index.php/Config.php
- https://github.com/leenooks/phpLDAPadmin/blob/master/config/config.php.example
- https://wiki.debian.org/PhpLdapAdmin
- http://phpldapadmin.sourceforge.net/wiki/index.php/Server:server:tls
