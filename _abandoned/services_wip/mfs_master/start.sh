#!/bin/bash
cp /etc/mfs/mfsmaster.cfg.sample /etc/mfs/mfsmaster.cfg
cp /etc/mfs/mfsexports.cfg.sample /etc/mfs/mfsexports.cfg

mfsmaster start -a
mfscgiserv -f
