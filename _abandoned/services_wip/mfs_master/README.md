# Installation
create necessary directories
```
mkdir -p /docker/mfs_master
mkdir -p /docker-volumes/mfs_master
```
copy necessary files, don't copy config files if you already have them
```
cp docker-compose.yml /docker/mfs_master/
cp Dockerfile /docker/mfs_master/
cp start.sh /docker/mfs_master/
```

last, start the server once, so docker takes care of restarting
```
cd /docker/mfs_master
docker-compose up -d
```

# Sources
- https://github.com/moosefs/moosefs-docker-cluster
- https://moosefs.com/faq/
