# Installation
create necessary directories
```
mkdir -p /docker/mfs_chunkserver
mkdir -p /docker-volumes/mfs_chunkserver
```
copy necessary files, don't copy config files if you already have them
```
cp docker-compose.yml /docker/mfs_chunkserver/
cp Dockerfile /docker/mfs_chunkserver/
cp start.sh /docker/mfs_chunkserver/
cp mfs_chunkserver.cfg /docker-volumes/mfs_chunkserver/
```

last, start the server once, so docker takes care of restarting
```
cd /docker/mfs_chunkserver
docker-compose up -d
```

# Sources
- https://github.com/moosefs/moosefs-docker-cluster
- https://moosefs.com/faq/
