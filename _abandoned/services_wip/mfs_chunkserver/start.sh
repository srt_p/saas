#!/bin/bash

mkdir -p /mnt/hdd0
chmod -R 777 /mnt/hdd0
chown -R mfs:mfs /mnt/hdd0

echo "/mnt/hdd0" >> /etc/mfs/mfshdd.cfg
echo "LABELS=$LABELS" >> /etc/mfs/mfschunkserver.cfg

mfschunkserver start -d
