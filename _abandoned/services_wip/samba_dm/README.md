# Description
This is a Samba Active Directory Domain Member service for the use with a separate Samba Domain Controller. This Domain Member provides network shares that themselves are mounted glusterfs volumes. The gluster-peers that provide the bicks for the volume are normally shut down to save power, so this container also provides glusterd and is added as a peer in the gluster.

# Installation
- **the host system must have fuse enabled**
  ```
  modprobe fuse
  echo fuse >> /etc/modules-load.d/modules.conf
  ```
- move this samba_dc-service folder to e.g. `/docker/samba_dc`
- adjust the configuration if needed
  - **HOSTS_ENTRY** is added to `etc/hosts` file, `getent hosts <hostname>` has to return the correct ip for the samba DNS
- start the server once, so docker takes care of restarting
  ```
  cd /docker/samba_dc
  docker-compose up -d
  ```
- if the server is not already a gluster peer, probe it from another gluster server and restart the container
- [TODO] mount error if not a gluster peer

# Sources
- https://wiki.samba.org/index.php/Setting_up_Samba_as_a_Domain_Member
- https://scriptthe.net/2015/02/05/autofs-in-docker-containers/
