#!/bin/sh

cat << EOF > /etc/krb5.conf
[libdefaults]
default_realm = ${REALM}
dns_lookup_realm = false
dns_lookup_kdc = true
EOF

echo "${HOSTS_ENTRY}" >> /etc/hosts

# gluster config
if [ ! -f /gluster/etc/glusterd.vol ]; then
    cp -rf /gluster_initial/etc /gluster/
    cp -rf /gluster_initial/lib /gluster/
fi
ln -s /gluster/etc /etc/glusterfs
ln -s /gluster/lib /var/lib/glusterd

# samba config
if [ -f /etc/samba/external/smb.conf ]; then
    cp -f /etc/samba/external/smb.conf /etc/samba/smb.conf
else
    cat << EOF > /etc/samba/smb.conf
[global]
    workgroup = ${DOMAIN}
    security = ADS
    realm = ${REALM}
    winbind refresh tickets = yes
    vfs objects = acl_xattr
    map acl inherit = yes
    store dos attributes = yes
    idmap config * : backend = tdb
    idmap config * : range = 3000-7999
    idmap config ${DOMAIN} : backend = rid
    idmap config ${DOMAIN} : range = 10000 - 999999
    template shell = /bin/bash
    template homedir = /home/%U
    interfaces = ${INTERFACE_IP}
    bind interfaces only = yes
    smb encrypt = required
    eventlog list = Application System Security SyslogLinux
EOF

    mkdir -p /var/lib/samba/private
    net ads join -U "administrator%${ADMINPASS}"

    cp /etc/samba/smb.conf /etc/samba/external/smb.conf
fi

glusterd -p /var/run/glusterd.pid

mount -t glusterfs -o acl localhost:/${GLUSTER_VOLUME} /shares
printf "\n${SMBSHARES}\n" >> /etc/samba/smb.conf

nmbd
winbindd

exec $@
