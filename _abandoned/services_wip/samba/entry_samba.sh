#!/bin/sh

cp /kgd.crt /usr/local/share/ca-certificates
update-ca-certificates

# create samba share folders according to smb.conf
for share in `grep "^[[:blank:]]*path" /etc/samba/smb.conf | sed -e 's/.*path = \(.*\)/\1/g'`; do
    mkdir -p $share
done

rm -rf /var/lib/samba/private/secrets.tdb
ln -s /secrets/secrets.tdb /var/lib/samba/private/secrets.tdb

winbindd
exec "$@"
