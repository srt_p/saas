# Description
This container is primarly intended as a NAS with user accounts stored in an LDAP-database and shares physically located in a glusterfs volume

# Installation
- **the host system must have fuse enabled**
  ```
  modprobe fuse
  echo fuse >> /etc/modules-load.d/modules.conf
  ```
- move this samba-service folder to e.g. `/docker/samba`
- adjust `docker-compose.yml`, especially the folder(s) to be shared
- adjust the rest of the configuration if needed
- copy your *.crt* file to `volumes`
- adjust the [Shares](#share-configuration)
- configure the ldap rootdn password, see [Usage](#usage)
- start the server once, so docker takes care of restarting. In case that the ldap database is not already containing samba-related configuration, head to [Usage](#usage) afterwards
  ```
  cd /docker/samba
  docker-compose up -d
  ```

# Share configuration
- if a single partition contains all shares, mount it to /media/share (or somewhere else, adjust in *docker-compose.yml*), e.g.
  ```
  mkdir -p /media/share
  echo "UUID=<uuid> /media/share ext4 rw,relatime 0 0" >> /etc/fstab
  reboot
  ```
- if multiple partitions contain the shares, they can be mounted for example with mergerfs (installed on host)
  ```
  mkdir -p /media/share
  echo "/media/share1:/media/share2 /media/share mergerfs 0 0" >> /etc/fstab
  reboot
  ```

# Usage
- set the rootdn password for ldap access
  ```
  docker-compose run samba smbpasswd -W
  ```
- initialize empty ldap database
  ```
  docker-compose run samba samba_ldap_provision
  ```
- add a user
  ```
  docker-compose exec samba samba_ldap_adduser <user>
  ```
- add user to group
  ```
  docker-compose exec samba samba_ldap_addmem <group> <user>
  ```

# Sources
- https://wiki.debian.org/LDAP/NSS
- https://wiki.debian.org/LDAP/PAM
- https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html
- https://wiki.samba.org/index.php/Ldapsam_Editposix
- https://wiki.samba.org/index.php/Samba_%26_LDAP
- https://wiki.samba.org/index.php/Setting_up_a_Share_Using_Windows_ACLs
