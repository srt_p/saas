# Installation
create necessary directories
```
mkdir -p /docker/pyload
mkdir -p /docker-volumes/pyload
```
copy necessary files, don't copy config files if you already have them
```
cp docker-compose.yml /docker/pyload/
cp Dockerfile /docker/pyload/
cp start-pyload.sh /docker/pyload/
cp setup.py /docker-volumes/pyload/
```
adjust the config files to fit your needs

the downloads are by default placed in /docker-volumes/pyload/downloads, you can for example mount a hard drive there, uuid can be found with 'blkid'
```
mkdir -p /docker-volumes/pyload/downloads
echo "UUID=<uuid> /docker-volumes/pyload/downloads ext4 rw,relatime 0 0" >> /etc/fstab
reboot
```

In docker-compose.yml adjust the <ip>-entry to fit the IP-Address where the Ports should be published.
Last, start the server once, so docker takes care of restarting
```
cd /docker/pyload
docker-compose up -d
```
