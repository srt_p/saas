# Description
this is an openldap-container designed for the use with samba

# Installation
- move this openldap-service folder to e.g. `/docker/openldap`
- adjust the configuration if needed
- comment port 389 in `docker-compose.yml` if you use `ldaps://`, otherwise comment port 636
- if `volumes/config` is empty or not existent, the slapd-configuration is generated from `volumes/slapd.ldif` when the container starts (remember to migrate afterwards)
- copy your *.crt* and *.key* files to `volumes`, also copy the CA certificate (see `docker-compose.yml`)
- start the server once, so docker takes care of restarting. Migration has to take place after the container started as it connects to the running server
  ```
  cd /docker/openldap
  docker-compose up -d
  ```
  if the `volumes/config` is regenerated (e.g. fresh install without adopted configuration or when configuration is rebuilt), execute the config-migration-script
  ```
  docker-compose exec openldap /scripts/migrate_config
  ```
  **set the root passwords**
  [TODO]

# Usage
## General information
- inside the container the `ldapi://` server access can be used, e.g.
  ```
  ldapwhoami -H ldapi:/// -Y EXTERNAL
  ```
- outside the container the `ldap://` or `ldaps://` server access can be used, e.g.
  ```
  ldapwhoami -H ldap://<host>/ -D "<rootdn>" -W
  # or
  ldapwhoami -H ldaps://<host>/ -D "<rootdn>" -W
  ```
  note: the `ldap://` and `ldaps://` access can also be used inside the container, for `ldaps://` the hostname has to be explicitly specified as the server dns name (respectively the DN of the certificate)

# Sources
- https://kb.sos-berlin.com/pages/viewpage.action?pageId=18778435
- https://www.digitalocean.com/community/tutorials/how-to-encrypt-openldap-connections-using-starttls
- https://serverfault.com/questions/587721/basic-openldap-setup-using-slapd-d-configuration
- https://wiki.archlinux.org/index.php/OpenLDAP#OpenLDAP_over_TLS
- https://openldap.org/doc/admin24/tls.html
- https://cyrusimap.org/sasl/sasl/authentication_mechanisms.html
- https://www.golinuxcloud.com/install-and-configure-openldap-centos-7-linux/
- https://cyrusimap.org/sasl/sasl/installation.html
- https://ldapcon.org/2011/downloads/Zeilenga-slides.pdf
- https://tools.ietf.org/html/rfc5803
- http://www.rfc-editor.org/rfc/rfc4422.txt
- https://www.redpill-linpro.com/techblog/2016/08/16/ldap-password-hash.html
- https://git.samba.org/samba.git/?p=samba.git;a=tree;f=examples/LDAP
- http://pig.made-it.com/samba-accounts.html
- https://openldap.org/doc/admin24/access-control.html
