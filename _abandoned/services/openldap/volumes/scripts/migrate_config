#!/bin/sh

# add a normal password to cn=config to be able to view
# and edit cn=config in a normal LDAP admin panel
if [ ! -z "$MIGRATE_CONFIG_ROOTPW" ] && [ "$MIGRATE_CONFIG_ROOTPW" -eq "1" ]; then
    {
        echo "dn: olcDatabase={0}config,cn=config"
        echo "changetype: modify"
        echo "replace: olcRootPW"
        echo "olcRootPW: $(slappasswd -u -n -s secret -h {CRYPT} -c '$6$%.16s')"
    } | ldapmodify -Q -Y EXTERNAL -H ldapi:/// | grep -v -e '^$'
fi

if [ ! -z "$MIGRATE_DATABASE_ROOTPW" ] && [ "$MIGRATE_DATABASE_ROOTPW" -eq "1" ]; then
    {
        echo "dn: olcDatabase={1}mdb,cn=config"
        echo "changetype: modify"
        echo "replace: olcRootPW"
        echo "olcRootPW: $(slappasswd -u -n -s secret -h {CRYPT} -c '$6$%.16s')"
    } | ldapmodify -Q -Y EXTERNAL -H ldapi:/// | grep -v -e '^$'
fi

if [ ! -z "$MIGRATE_USE_LDAPS" ] && [ "$MIGRATE_USE_LDAPS" -eq "1" ]; then
    {
        echo "dn: cn=config"
        echo "changetype: modify"
        echo "replace: olcTLSCACertificateFile"
        echo "olcTLSCACertificateFile: ${OPENLDAP_CA_CRT_FILE}"
        echo "-"
        echo "replace: olcTLSCertificateFile"
        echo "olcTLSCertificateFile: ${OPENLDAP_CRT_FILE}"
        echo "-"
        echo "replace: olcTLSCertificateKeyFile"
        echo "olcTLSCertificateKeyFile: ${OPENLDAP_KEY_FILE}"
        echo "-"
        echo "replace: olcTLSVerifyClient"
        echo "olcTLSVerifyClient: allow"
        echo "-"
        echo "replace: olcTLSCipherSuite"
        echo "olcTLSCipherSuite: HIGH:MEDIUM:-SSLv2:-SSLv3"
    } | ldapmodify -Q -Y EXTERNAL -H ldapi:/// | grep -v -e '^$'
fi

if [ ! -z "$MIGRATE_DISABLE_ANONYMOUS" ] && [ "$MIGRATE_DISABLE_ANONYMOUS" -eq "1" ]; then
    {
        echo "dn: cn=config"
        echo "changetype: modify"
        echo "replace: olcDisallows"
        echo "olcDisallows: bind_anon"
        echo "-"
        echo "replace: olcRequires"
        echo "olcRequires: authc"
        echo ""
        echo "dn: olcDatabase={-1}frontend,cn=config"
        echo "changetype: modify"
        echo "replace: olcRequires"
        echo "olcRequires: authc"
    } | ldapmodify -Q -Y EXTERNAL -H ldapi:/// | grep -v -e '^$'
fi

# set default password hash to crypt
{
    echo "dn: cn=config"
    echo "changetype: modify"
    echo "replace: olcPasswordHash"
    echo "olcPasswordHash: {CRYPT}"
    echo "-"
    echo "replace: olcPasswordCryptSaltFormat"
    echo "olcPasswordCryptSaltFormat: \$6\$%.16s"
} | ldapmodify -Q -Y EXTERNAL -H ldapi:/// | grep -v -e '^$'
