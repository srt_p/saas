#!/bin/sh

if [ ! "$(ls -A /etc/openldap/slapd.d)" ]; then
    echo "/etc/openldap/slapd.d empty, fill with initial information"
    sed -e "s|{{SLAPD_SUFFIX}}|$SLAPD_SUFFIX|g" \
        -e "s|{{SLAPD_ROOTDN}}|$SLAPD_ROOTDN|g" \
        /slapd.ldif.template > /slapd.ldif
    slapadd -n0 -F /etc/openldap/slapd.d -l /slapd.ldif
fi

chmod 700 /scripts/*

cp -f ${OPENLDAP_CA_CRT_FILE} /usr/local/share/ca-certificates/
echo "INFO: ignore following warning about ca-certificates.crt"
update-ca-certificates
chmod 444 ${OPENLDAP_CA_CRT_FILE}
chmod 444 ${OPENLDAP_CRT_FILE}
chmod 400 ${OPENLDAP_KEY_FILE}

if [ "$MIGRATE_USE_LDAPS" -eq "1" ]; then
    slapd -F /etc/openldap/slapd.d -d $OPENLDAP_LOG_LEVEL -h "ldapi:/// ldaps:///"
else
    slapd -F /etc/openldap/slapd.d -d $OPENLDAP_LOG_LEVEL -h "ldapi:/// ldap:///"
fi
