#!/usr/bin/env python3

import json
import subprocess
import pydhcpdparser
import re
import web
web.template.ALLOWED_AST_NODES.append('Constant')
web.template.ALLOWED_AST_NODES.append('Starred')

render = web.template.render('templates/')

urls = (
    '/', 'index',
    '/ping_(\d\d?\d?\.\d\d?\d?\.\d\d?\d?\.\d\d?\d?)', 'ping',
    '/wol_([a-f0-9][a-f0-9]:[a-f0-9][a-f0-9]:[a-f0-9][a-f0-9]:[a-f0-9][a-f0-9]:[a-f0-9][a-f0-9]:[a-f0-9][a-f0-9])', 'wol',
    '/refresh_hosts', 'refresh_hosts'
)

class index:
    def GET(self):
        with open('/wolweb/wolweb.conf') as wol_web_conf_file:
            wol_web_conf = json.load(wol_web_conf_file)

        host_ranges = [[ip_to_int(i[0]), ip_to_int(i[1])] for i in wol_web_conf['hosts']]
        service_ranges = [[ip_to_int(i[0]), ip_to_int(i[1])] for i in wol_web_conf['services']]

        with open('/dhcpd-hosts.conf', 'r') as dhcpd_conf_file:
            dhcpd_conf_str = dhcpd_conf_file.read()
        dhcpd_conf = pydhcpdparser.parser.parse(dhcpd_conf_str)

        hosts = {i[0]: i[1] for i in dhcpd_conf[0]['host'].items() if ip_in_ranges(i[1]['fixed-address'], host_ranges)}
        services = {i[0]: i[1] for i in dhcpd_conf[0]['host'].items() if ip_in_ranges(i[1]['fixed-address'], service_ranges)}
        return render.index(hosts, services)

class ping:
    def POST(self, ip):
        web.header('Content-type', 'application/json')
        # fping doesn't need root privileges unlike ping
        cmd = 'fping -c 1 ' + ip
        popen = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        popen.wait()
        is_up = (popen.returncode == 0)
        return json.dumps({'ip': ip, 'is_up': is_up})

class wol:
    def POST(self, mac):
        with open('/wol_pipe/macs', 'a') as pipe:
            pipe.write(mac + '\n')
        raise web.seeother('/')

class refresh_hosts:
    def POST(self):
        subprocess.run(['/get_dhcpd_hosts.sh'])
        raise web.seeother('/')

def ip_to_int(ip):
    m = re.search('(\d+)\.(\d+)\.(\d+)\.(\d+)', ip)
    return (int(m.group(1)) << 24) + (int(m.group(2)) << 16) + (int(m.group(3)) << 8) + int(m.group(4))

def ip_in_ranges(ip, ranges):
    int_ip = ip_to_int(ip)
    for r in ranges:
        if int_ip >= r[0] and int_ip <= r[1]:
            return True
    return False

if __name__ == '__main__':
    app = web.application(urls, globals())
    app.run()
