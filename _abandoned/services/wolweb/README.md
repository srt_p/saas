# Description
a webserver providing a website that lists *hosts* (e.g. physical devices) and *services* (e.g. virtual servers), shows their availability-status and provides buttons for awaking non-pingable hosts with Wake-On-Lan

# Installation
- move this wolweb-service folder to e.g. `/docker/wolweb`
- adjust *<ip>* and other stuff in `docker-compose.yml`
- adjust the rest of the configuration if needed
- the `wolweb.conf`-file specifies the IP-Ranges of the hosts and services, an example would be
  ```
  {
      "hosts": [
          ["192.168.1.0", "192.168.1.255"],
          ["192.168.5.0", "192.168.10.56"]
      ],
      "services": [
          ["192.168.2.0", "192.168.2.255"]
      ]
  }
  ```
- copy your *.crt* and *.key* files to `volumes/wolweb`
- copy the *ssh-id* for accessing the dhcp-server to `volumes/wolweb`
- start the server once, so docker takes care of restarting
  ```
  cd /docker/wolweb
  docker-compose up -d
  ```

# Sources
- https://github.com/webpy/webpy.github.com/blob/master/cookbook/fastcgi-lighttpd.md
- https://github.com/m4rcu5nl/docker-lighttpd-alpine
