#!/bin/sh
  
# cron for getting the dhcp config file
crond

# add dhcp server to known hosts
ssh-keyscan -p $DHCPD_SSH_PORT $DHCPD_SSH_HOST >> /etc/ssh/global_known_hosts
echo "CheckHostIP no" >> /etc/ssh/ssh_config
echo "GlobalKnownHostsFile /etc/ssh/global_known_hosts" >> /etc/ssh/ssh_config

# ssh key preparation
cp -f $DHCPD_SSH_ID ${DHCPD_SSH_ID}_lighttpd
chmod 400 $DHCPD_SSH_ID
chown lighttpd:lighttpd ${DHCPD_SSH_ID}_lighttpd
chmod 400 ${DHCPD_SSH_ID}_lighttpd

# copy initial dhcp config file
/usr/bin/scp -q -i $DHCPD_SSH_ID -P $DHCPD_SSH_PORT $DHCPD_SSH_USER@$DHCPD_SSH_HOST:/etc/dhcp/dhcpd-hosts.conf /dhcpd-hosts.conf
chmod 666 /dhcpd-hosts.conf

# create the pipe for wake on lan
mkfifo /wol_pipe/macs
chown lighttpd:lighttpd /wol_pipe/macs

chmod 777 /var/www/code.py
if [ ! -f /wolweb/log/lighttpd_error.log ]; then
    touch /wolweb/log/lighttpd_error.log
fi
chmod 666 /wolweb/log/lighttpd_error.log

cat $WOLWEB_KEY $WOLWEB_CRT > /wolweb.pem
/usr/sbin/lighttpd -D -f /etc/lighttpd/lighttpd.conf
