#!/bin/sh
if [ $(id -u) -eq 0 ]; then
    /usr/bin/scp -q -i $DHCPD_SSH_ID -P $DHCPD_SSH_PORT $DHCPD_SSH_USER@$DHCPD_SSH_HOST:/etc/dhcp/dhcpd-hosts.conf /dhcpd-hosts.conf
else
    /usr/bin/scp -q -i ${DHCPD_SSH_ID}_lighttpd -P $DHCPD_SSH_PORT $DHCPD_SSH_USER@$DHCPD_SSH_HOST:/etc/dhcp/dhcpd-hosts.conf /dhcpd-hosts.conf
fi
