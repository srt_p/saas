#!/bin/sh

pacman -Syu --noconfirm
pacman -S --needed --noconfirm base-devel openssh vim htop tmux ethtool sudo

# persistent wol service
cat > /etc/systemd/system/wol@.service << EOF
[Unit]
Description=Wake-on-LAN for %i
Requires=network.target
After=network.target

[Service]
ExecStart=/usr/bin/ethtool -s %i wol g
Type=oneshot

[Install]
WantedBy=multi-user.target
EOF

# automatic shutdown script
cat > /usr/bin/iscsi_target_auto_shutdown << EOF
#!/bin/bash
check_iscsi_active () {
    echo checking iscsi
    if [ "$(targetcli sessions detail)" != "(no open sessions)" ]; then
        exit 0
    fi
}
check_who_active () {
    echo checking who
    if [ -n "$(who)" ]; then
        exit 0
    fi
}
for i in 1 2 3 4; do
    echo "waiting interval $i"
    sleep 1m
    check_iscsi_active
    check_who_active
done
echo shutdown
poweroff
EOF
chmod +x /usr/bin/iscsi_target_auto_shutdown

# automatic shutdown service
cat > /etc/systemd/system/iscsi_target_auto_shutdown.service << EOF
[Unit]
Description=automatic shutdown if no iscsi connection or login is active

[Service]
ExecStart=/usr/bin/iscsi_target_auto_shutdown
Restart=always

[Install]
WanteBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable --now iscsi_target_auto_shutdown

# targetcli
mkdir -p /targetcli
git clone https://aur.archlinux.org/python-configshell-fb.git /targetcli/python-configshell
git clone https://aur.archlinux.org/python-rtslib-fb.git /targetcli/python-rtslib
git clone https://aur.archlinux.org/targetcli-fb.git /targetcli/targetcli
chmod -R 777 /targetcli

cd /targetcli/python-configshell
pacman -S --needed --noconfirm python-pyparsing python-urwid python-setuptools python2-setuptools
sudo -u nobody makepkg -c
pacman -U --noconfirm python-configshell-fb-*.tar.xz

cd /targetcli/python-rtslib
pacman -S --needed --noconfirm python python-pyudev python-six python-pyudev python-setuptools
sudo -u nobody makepkg -c
pacman -U --noconfirm python-rtslib-fb-*.tar.xz

cd /targetcli/targetcli
pacman -S --needed --noconfirm python-dbus python-gobject python-setuptools python-ethtool
# this patch is currently needed
sed -i -e 's|^\(\s*\)\(python setup.py build\)$|\1sed -i s-/lib/systemd/system-lib/systemd/system-g setup.py\n\1\2|g' PKGBUILD
sudo -u nobody makepkg -c
pacman -U --noconfirm targetcli-fb-*.tar.xz

cd /
rm -rf /targetcli

systemctl enable --now target.service
