# Description
for the PCs publishing iscsi-targets. Each of the pcs publishes the data draves as target.

# Installation
install Archlinux

# Postinstall
install git and clone this repository
```
pacman -S git
git clone ...
```
execute post install scripts
```
setup01.sh  # additional packages
setup02.sh  # additional user 'alarm'
setup03.sh  # additional configuration
```
enable persistent wake on lan
```
systemctl enable wol@<interface>.service
systemctl start wol@<interface>.service
```
reboot and configure the iscsi targets (see Usage -> iscsi-server)

# Usage
- iscsi-server
  - create targets
    ```
    targetcli
    ```
    - include a block device for every target disk
      ```
      cd backstores/block
      create md_block<#block> /dev/disk/by-id/<md-name>
      create ...
      ```
    - create ISCSI Qualified Name (IQN)
      ```
      cd /iscsi
      create
      ```
    - disable CHAP
      ```
      cd iqn.../tpg1
      set attribute authentication=0
      ```
    - add acl (iqn of initiator)
      ```
      cd acls
      create <iqn>
      ```
    - add every disk to target LUN
      ```
      cd ../luns
      create /backstores/block/<block>
      create ...
      ```
    - **save configuration**
      ```
      cd /
      saveconfig
      ```
  - reload target service
    ```
    systemctl restart target.service
    ```
  - list sessions
    ```
    targetcli sessions
    ```
- iscsi-client
  - kill iscsi session
    ```
    iscsiadm -m node -T <iqn> -p <ip>:<port> -u
    ```
  - remove iscsi node
    ```
    iscsiadm -m node -o delete -T <iqn>
    ```
  - remove record from discoverydb
    ```
    iscsiadm -m discoverydb -t sendtargets -p <ip>:<port> -o delete
    ```

# Sources
- https://www.server-world.info/en/note?os=Fedora_30&p=iscsi&f=2
- https://www.howtoforge.com/how-to-setup-iscsi-storage-server-on-ubuntu-1804/
- https://support.unitrends.com/UnitrendsBackup/s/article/000003999
- https://www.docker.com/blog/road-to-containing-iscsi/
- https://wiki.archlinux.org/index.php/ISCSI/LIO#Tips_&_Tricks
