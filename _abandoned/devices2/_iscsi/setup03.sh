#!/bin/sh

# empty banner
echo "" > /etc/motd

# ssh configuration
if [ ! -f /home/alarm/.ssh/authorized_keys ]; then
    echo ""
    echo "error: first copy your public ssh key to /home/alarm/.ssh/authorized_keys"
    exit
fi
chmod 700 /home/alarm/.ssh
chmod 600 /home/alarm/.ssh/authorized_keys
chown -R alarm:alarm /home/alarm/.ssh

cat > /etc/ssh/sshd_config << EOL
AllowUsers alarm
PermitRootLogin no
AuthorizedKeysFile .ssh/authorized_keys
PasswordAuthentication no
ChallengeResponseAuthentication no
PrintMotd no
Subsystem sftp /usr/lib/ssh/sftp-server
EOL

systemctl stop sshd.service
systemctl enable sshd.service
systemctl start sshd.service

# tmux configuration
cat > /etc/tmux.conf << EOL
set -g default-terminal "screen-256color"
set -g mouse on
EOL

# vim configuration
cat > /root/.vimrc << EOL
set tabstop=4
set softtabstop=0
set expandtab
set shiftwidth=4
set smarttab
set mouse=a
EOL

# grub configuration
if [ -f /etc/default/grub ]; then
    sed -i -e 's/^GRUB_TIMEOUT=.*/GRUB_TIMEOUT=0/' /etc/default/grub
    grub-mkconfig -o /boot/grub/grub.cfg
fi
