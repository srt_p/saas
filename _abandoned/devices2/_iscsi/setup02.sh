#!/bin/sh

groupadd alarm
useradd -m -g alarm -G wheel alarm
# password is necessary that account will not be locked
echo enter password for user alarm
passwd alarm
echo "root ALL=(ALL) ALL" > /etc/sudoers
echo "Defaults:%wheel targetpw" >> /etc/sudoers
echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers
