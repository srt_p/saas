#!/bin/sh

# don't need docker (until now)
systemctl disable --now docker.service

pacman -S --needed --noconfirm base-devel openssh open-iscsi python python-pip python-xattr

# python packages
python3 -m pip install --upgrade pip
python3 -m pip install ping3 pywol

# mergerfs
git clone https://github.com/trapexit/mergerfs.git /mergerfs
cd /mergerfs
make
make PREFIX="/usr" SBINDIR="/usr/bin" install
cd /
rm -rf /mergerfs

# micluster
git clone https://gitlab.com/srt_p/micluster.git /micluster
cp -f /micluster/micluster /usr/bin/micluster
cp -f /micluster/miclusterctl /usr/bin/miclusterctl
chmod +x /usr/bin/micluster
chmod +x /usr/bin/miclusterctl
rm -rf /micluster
mkdir -p /etc/micluster

# micluster service
cat > /etc/systemd/system/micluster@.service << EOL
[Unit]
Description=micluster for cluster '%i'

[Service]
ExecStart=/usr/bin/micluster %i

[Install]
WantedBy=multi-user.target
EOL
systemctl daemon-reload

systemctl enable --now iscsid.service
