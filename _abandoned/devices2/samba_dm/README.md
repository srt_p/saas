# Description
This samba domain member functions as a file server for the AD-Accounts that are created and managed on the domain controller. The drives to be shared are mapped from other pcs using [micluster](https://gitlab.com/srt_p/micluster). The samba shares are normally subdirectories of this mounted folder.
### how micluster is started
this samba_dm configuration installs a simple http server, when this server receives a GET request on http://<host>/start 
### how micluster is stopped
sambas full_audit mode allows logging of multiple events on the share. syslog-ng sends the logs to samba_dm_stopper. If no log is received in the specified timeout, samba_dm_stopper stops the systemd service. The audit events are those that seem to typically happen on normal share access (browsing, reading, writing, ...), but excluding the events that happen, when the share is not accessed directly, but only when opening Windows Explorer.

# Installation
see **Installation** and **Postinstall** of [archlinuxarm](../_rpi/archlinuxarm)

# Postinstall
execute post install scripts, you may want to get DHCP (and DNS, other from samba) for the device right
```
setup04.sh  # micluster installation
setup05.sh  # samba configuration
```

# Configuration
- set iscsi initiator name (this has to be set in iscsi target acls), e.g.
  <pre>
  /etc/iscsi/initiatorname.iscsi<hr>InitiatorName=<i>iqn.2020-12.srt.shares</i>
  </pre>
  and restart iscsid.service
- configure micluster, see [here](https://gitlab.com/srt_p/micluster). If the iscsi target setup of this repository is used the iscsi targets shut down automatically, no ssh config for micluster is needed
- the managed micluster clusters are configured in `/etc/samba_dm/samba_dm.conf`, e.g.
  ```
  {
      "clusters": ["data1", "data2"],
      "timeout": 2700
  }
  ```
  explanation:
  - clusters: the micluster clusters to be started and stopped
  - timeout: seconds after which the clusters should be stopped after no access
- configure samba shares, append to `/etc/samba/smb.conf`, e.g.
  ```
  [share]
      path = /shares/<share>
      read only = no
      #browseable = no
  ```
- **with micluster started** add the directory, set permissions and reload samba. Here is an example for rwx for Domain Admins and rx for Domain Users
  ```
  mkdir /shares/<share>
  chmod -R 2770 /shares/<share>

  setfacl -Rm group::--- /shares/<share>
  setfacl -Rm other::--- /shares/<share>
  setfacl -Rm default:group::--- /shares/<share>
  setfacl -Rm default:other::--- /shares/<share>

  setfacl -Rm group:"<domain>\\Domain Admins":rwx /shares/<share>
  setfacl -Rm group:"<domain>\\Domain Users":r-x /shares/<share>

  setfacl -Rm default:group:"<domain>\\Domain Admins":rwx /shares/<share>
  setfacl -Rm default:group:"<domain>\\Domain Users":r-x /shares/<share>

  smbcontrol all reload-config
  ```

# Usage
- to connect to the shares enter the username as DOMAIN\username

# Sources
- https://wiki.samba.org/index.php/Setting_up_Samba_as_a_Domain_Member
- https://wiki.samba.org/index.php/Setting_up_a_Share_Using_POSIX_ACLs
- https://wiki.samba.org/index.php/Setting_up_a_Share_Using_Windows_ACLs
- https://wiki.samba.org/index.php/Troubleshooting_Samba_Domain_Members#The_net_Command_Fails_to_Connect_to_the_127.0.0.1_IP_Address
- https://scriptthe.net/2015/02/05/autofs-in-docker-containers/
- https://www.rsyslog.com/doc/v8-stable/configuration/modules/omprog.html
- https://www.samba.org/samba/docs/current/man-html/vfs_full_audit.8.html
- https://lists.samba.org/archive/samba/2017-March/207489.html
