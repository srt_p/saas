#!/bin/sh

pacman -S --needed --noconfirm samba syslog-ng lighttpd

read -ep "this machine hostname (e.g. dm1): " hostname
read -ep "this machine new dns name (e.g. dm1.ad.example.com): " host
read -ep "Samba REALM (e.g. AD.EXAMPLE.COM): " realm
read -ep "Samba DOMAIN (e.g. SAMDOM): " domain

mkdir -p /etc/samba_dm/http

# hosts configuration
ip=$(ip r get 1 | sed -n 's/^.*src \([0-9.]*\).*$/\1/p')
shorthost=$(sed 's/[.].*//g' <<< $host)
hostnamectl set-hostname "${hostname}"
echo "127.0.0.1 localhost" > /etc/hosts
echo "::1 localhost" >> /etc/hosts
echo "${ip} ${host} ${shorthost}" >> /etc/hosts

# kerberos configuration
cat << EOL > /etc/krb5.conf
[libdefaults]
default_realm = ${realm}
dns_lookup_realm = false
dns_lookup_kdc = true
EOL

# samba configuration
cat << EOL > /etc/samba/smb.conf
[global]
    workgroup = ${domain}
    security = ADS
    realm = ${realm}

    access based share enum = yes
    acl allow execute always = yes
    inherit acls = yes
    template homedir = /home/%U
    template shell = /bin/bash
    winbind refresh tickets = yes
    winbind use default domain = yes

    idmap config * : backend = tdb
    idmap config * : range = 3000-7999
    idmap config ${domain} : backend = rid
    idmap config ${domain} : range = 10000-999999

    vfs objects = full_audit
    full_audit : prefix =
    full_audit : success = pwrite_send pwrite_recv pread_send pread_recv kernel_flock getxattr strict_lock_check setxattr set_dos_attributes fchown listxattr fdopendir readdir closedir telldir opendir
    full_audit : failed = none
    full_audit : facility = local7
    full_audit : priority = NOTICE
EOL

echo "Joining Domain"
read -ep "Admin User: " user
net ads join -U "${user}"

systemctl enable --now nmb.service
systemctl enable --now winbind.service
systemctl enable --now smb.service

# samba_dm_starter
cat > /etc/lighttpd/lighttpd.conf << EOL
server.modules = ( "mod_cgi" )
server.port = 80
server.username = "http"
server.groupname = "http"
server.errorlog = "/var/lig/lighttpd/error.log"
server.document-root = "/etc/samba_dm/http"
$HTTP["url"] == "/start" {
    cgi.assign = ( "" => "" )
}
EOL
cat > /etc/samba_dm/http/start << EOL
#!/bin/python3
import subprocess, json
lClusters = []
with open('/etc/samba_dm/samba_dm.conf', 'r') as file:
    lClusters = json.load(file)["clusters"]
for sCluster in lClusters:
    subprocess.run(['/usr/bin/sudo', '/usr/bin/systemctl', 'start', "micluster@{}".format(sCluster)])
print("starting")
EOL
chmod +x /etc/samba_dm/http/start
echo "http ALL=(ALL) NOPASSWD: /usr/bin/systemctl start micluster@*" >> /etc/sudoers
systemctl enable --now lighttpd.service

# samba_dm_stopper
cat > /usr/bin/samba_dm_stopper << EOL
import json, signal, subprocess
lClusters = []
iTimeout = 900
with open('/etc/samba_dm/samba_dm.conf', 'r') as file:
    dConfig = json.load(file)
    lClusters = dConfig["clusters"]
    iTimeout = dConfig["timeout"]
def interrupt(sig, frame):
    raise Exception("timeout")
signal.signal(signal.SIGALRM, interrupt)
while True:
    try:
        signal.alarm(iTimeout)
        input()
        signal.alarm(0)
    except:
        break
for sCluster in lClusters:
    subprocess.run(['/usr/bin/systemctl', 'stop', "micluster@{}".format(sCluster)])
EOL
chmod +x /usr/bin/samba_dm_stopper

# syslog-ng configuration
cat > /etc/syslog-ng/syslog-ng.conf << EOL
@version: 3.29
@include "scl.conf"
options {
    mark-freq(0);
};
source s_local {
    system();
    internal();
};
destination d_samba_dm_stopper {
    program("/usr/bin/samba_dm_stopper" template("\${DATE} \${MESSAGE}\n"));
};
filter f_local7 {
    facility(local7);
};
log {
    source(s_local);
    filter(f_local7);
    destination(d_samba_dm_stopper);
};
EOL
systemctl enable --now syslog-ng@default.service

# nsswitch configuration
sed -i 's/\(passwd:.*\)/\1 winbind/g' /etc/nsswitch.conf
sed -i 's/\(group:.*\)/\1 winbind/g' /etc/nsswitch.conf
