#!/bin/sh

# check if user 'alarm' already exists, which should be the case for ArchlinuxARM on Raspberry Pi
getent passwd alarm > /dev/null 2&>1
if [ $? -ne 0 ]; then
    groupadd alarm
    useradd -m -g alarm -G wheel alarm
    # password is necessary that account will not be locked
    echo enter password for user alarm
    passwd alarm
fi
echo "root ALL=(ALL) ALL" > /etc/sudoers
echo "Defaults:%wheel targetpw" >> /etc/sudoers
echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers
