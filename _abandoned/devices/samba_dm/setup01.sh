#!/bin/sh

pacman -Syu --noconfirm
pacman -S --needed --noconfirm base-devel openssh vim htop cronie tmux samba parted open-iscsi python-pip fuse2 syslog-ng sudo python-xattr

# python packages
python3 -m pip install --upgrade pip
python3 -m pip install pydhcpdparser ping3 pywol

# current directory
script=$(readlink -f "$0")
path=$(dirname "$script")

# samba_dm
cp -f "${path}/samba_dm_get_dhcpd_hosts" /usr/bin
cp -f "${path}/samba_dm_watcher" /usr/bin/
cp -f "${path}/samba_dm_starter" /usr/bin/
chmod +x /usr/bin/samba_dm_get_dhcpd_hosts
chmod +x /usr/bin/samba_dm_watcher
chmod +x /usr/bin/samba_dm_starter
mkdir -p /etc/samba_dm
mkdir -p /var/log/samba_dm

# mergerfs
git clone https://github.com/trapexit/mergerfs.git /mergerfs
cd /mergerfs
make
make PREFIX="/usr" SBINDIR="/usr/bin" install
cd /
rm -rf /mergerfs

# enable and start services
systemctl enable cronie.service
systemctl start cronie.service
systemctl enable iscsid.service
systemctl start iscsid.service
