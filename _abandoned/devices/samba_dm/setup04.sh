#!/bin/sh

read -ep "this machine hostname (e.g. dm1): " hostname
read -ep "this machine new dns name (e.g. dm1.ad.example.com): " host
read -ep "Samba REALM (e.g. AD.EXAMPLE.COM): " realm
read -ep "Samba DOMAIN (e.g. SAMDOM): " domain

# hosts configuration
ip=$(ip r get 1 | sed -n 's/^.*src \([0-9.]*\).*$/\1/p')
shorthost=$(sed 's/[.].*//g' <<< $host)
hostnamectl set-hostname "${hostname}"
echo "127.0.0.1 localhost" > /etc/hosts
echo "::1 localhost" >> /etc/hosts
echo "${ip} ${host} ${shorthost}" >> /etc/hosts

# kerberos configuration
cat << EOL > /etc/krb5.conf
[libdefaults]
default_realm = ${realm}
dns_lookup_realm = false
dns_lookup_kdc = true
EOL

# samba configuration
cat << EOL > /etc/samba/smb.conf
[global]
    workgroup = ${domain}
    security = ADS
    realm = ${realm}

    access based share enum = yes
    acl allow execute always = yes
    inherit acls = yes
    template homedir = /home/%U
    template shell = /bin/bash
    winbind refresh tickets = yes

    idmap config * : backend = tdb
    idmap config * : range = 3000-7999
    idmap config ${domain} : backend = rid
    idmap config ${domain} : range = 10000-999999

    vfs objects = full_audit
    full_audit : prefix =
    full_audit : success = pwrite_send pwrite_recv pread_send pread_recv kernel_flock getxattr strict_lock_check setxattr set_dos_attributes fchown listxattr fdopendir readdir closedir telldir opendir
    full_audit : failed = none
    full_audit : facility = local7
    full_audit : priority = NOTICE
EOL

echo "Joining Domain"
read -ep "Admin User: " user
net ads join -U "${user}"

systemctl enable nmb.service
systemctl start nmb.service
systemctl enable winbind.service
systemctl start winbind.service
systemctl enable smb.service
systemctl start smb.service
