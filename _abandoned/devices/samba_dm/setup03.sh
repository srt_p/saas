#!/bin/sh

# empty banner
echo "" > /etc/motd

# ssh configuration
if [ ! -f /home/alarm/.ssh/authorized_keys ]; then
    echo ""
    echo "error: first copy your public ssh key to /home/alarm/.ssh/authorized_keys"
    exit
fi
chmod 700 /home/alarm/.ssh
chmod 600 /home/alarm/.ssh/authorized_keys
chown -R alarm:alarm /home/alarm/.ssh

cat > /etc/ssh/sshd_config << EOL
AllowUsers alarm
PermitRootLogin no
AuthorizedKeysFile .ssh/authorized_keys
PasswordAuthentication no
ChallengeResponseAuthentication no
PrintMotd no
Subsystem sftp /usr/lib/ssh/sftp-server
EOL

cat > /etc/ssh/ssh_config << EOL
StrictHostKeyChecking no
GlobalKnownHostsFile /dev/null
EOL

systemctl stop sshd.service
systemctl enable sshd.service
systemctl start sshd.service

# tmux configuration
cat > /etc/tmux.conf << EOL
set -g default-terminal "screen-256color"
set -g mouse on
EOL

# vim configuration
cat > /root/.vimrc << EOL
set tabstop=4
set softtabstop=0
set expandtab
set shiftwidth=4
set smarttab
set mouse=a
EOL

# grub configuration
if [ -f /etc/default/grub ]; then
    sed -i -e 's/^GRUB_TIMEOUT=.*/GRUB_TIMEOUT=0/' /etc/default/grub
    grub-mkconfig -o /boot/grub/grub.cfg
fi

# cron configuration
echo "0 */4 * * * /usr/bin/samba_dm_get_dhcpd_hosts" | crontab -

# syslog-ng configuration
cat > /etc/syslog-ng/syslog-ng.conf << EOL
@version: 3.25
@include "scl.conf"
source s_local {
    system();
    internal();
};
source s_smbd {
    file("/var/log/samba/log.smbd" follow-freq(2));
};
destination d_samba_dm_watcher {
    program("/usr/bin/samba_dm_watcher" template("\${DATE} \${MESSAGE}\n"));
};
destination d_samba_dm_starter {
    program("/usr/bin/samba_dm_starter" template("\${DATE} \${MESSAGE}\n"));
};
filter f_local7 {
    facility(local7);
};
log {
    source(s_local);
    filter(f_local7);
    destination(d_samba_dm_watcher);
};
log {
    source(s_smbd);
    destination(d_samba_dm_starter);
};
EOL
systemctl enable syslog-ng@default.service
systemctl start syslog-ng@default.service

# nsswitch configuration
sed -i 's/\(passwd:.*\)/\1 winbind/g' /etc/nsswitch.conf
sed -i 's/\(group:.*\)/\1 winbind/g' /etc/nsswitch.conf
