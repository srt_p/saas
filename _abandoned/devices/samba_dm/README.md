# Description
This samba domain member functions as a file server for the AD-Accounts that are created and managed on the domain controller. The drives to be shared are mapped from other pcs (that can be automatically shut down to save power) with iscsi. Those drives are then mapped with mergerfs to one folder (e.g. */shares*).The samba shares are then subdirectories of this folder. If the target servers are not pingable a Wake-on-LAN package is sent.
### How iscsi-targets are started
When a Samba-Share is accessed and its path is not available, samba issues an error to its log file, syslog-ng sends the logs to the samba_dm_starter and if the respective error message is found, the iscsi-targets are started
### How iscsi-targets are stopped
Sambas full_audit mode allows logging of multiple events on the share. samba_dm_watcher watches for events that seem to typically happen on normal share access (browsing, reading, writing, ...), but excluding the events that happen, when the share is not accessed directly, but only when opening Windows Explorer.

# Installation
install Archlinux
- for Raspberry Pi 4 devices see: https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-4
- for Raspberry Pi 3 devices see: https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3
- ssh to the raspberry and change root password
  ```
  ssh alarm@<ip>
  su - root # default password is 'root'
  passwd
  pacman-key --init
  pacman-key --populate archlinuxarm
  pacman -Syu
  ```

# Postinstall
set timezone
```
ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
```
install git and clone this repository
```
pacman -S git
git clone ...
```
execute post install scripts, before running setup04 you may want to get DHCP (and DNS, other from Samba) for the device right
```
setup01.sh  # additional packages
setup02.sh  # additional user 'alarm'
setup03.sh  # additional configuration
setup04.sh  # samba configuration
```
configure samba_dm (see Usage) and get the dhcp-host config once (**after configuration**)
```
samba_dm_get_dhcdp_hosts
```

# Usage
- for retrieving the dhcpd host config file from the dhcp server (for the MAC-Adresses to send WOL-packages), scp is used. Copy the ssh private key to this machine and add the path to the scp configuration file.
  ```
  cp path/to/key/... /home/alarm/.ssh/...
  chown -R alarm:alarm /home/alarm/.ssh
  chmod 600 /home/alarm/.ssh/...
  ```
- configuration
  - iscsi initiator name: `/etc/iscsi/initiatorname.iscsi`
    ```
    InitiatorName=<iqn>
    ```
  - samba_dm: `/etc/samba_dm/config` in json-style, e.g.
    ```
    {
      "targets": [
        {
          "host": "<hostname, e.g. iscsi0.rz.kgd>",
          "port": "<port for ssh access, e.g. 22>",
          "id": "<path to ssh id file>",
          "user": "<username for ssh access>"
        },
        ...
      ],
      "destination": "/shares",
      "tmp_destination": "/mnt",
      "mergerfs_options": ["security_capability=false"],
      "ping_timeout": 2,
      "wol_timeout": 10,
      "shutdown_timeout": 600,
      "debug": 0
    }
    ```
  - scp (for dhcp-server ssh access): `/etc/samba_dm/scp`
    ```
    DHCPD_SSH_ID=<private key file without password>
    DHCPD_SSH_PORT=<ssh port>
    DHCPD_SSH_USER=<ssh user>
    DHCPD_SSH_HOST=<ssh host>
    ```
  - samba shares: append to `/etc/samba/smb.conf`, e.g.
    ```
    [share]
        path = /shares/share
        read only = no
        #browseable = no
        #valid users = ...
    ```
    also add the directory, set permissions and reload samba. Here an example for rwx for Domain Admins and rx for Domain Users
    ```
    mkdir /shares/<share>
    chmod -R 2770 /shares/<share>

    setfacl -Rm group::--- /shares/<share>
    setfacl -Rm other::--- /shares/<share>
    setfacl -Rm default:group::--- /shares/<share>
    setfacl -Rm default:other::--- /shares/<share>

    setfacl -Rm group:"<domain>\\Domain Admins":rwx /shares/<share>
    setfacl -Rm group:"<domain>\\Domain Users":r-x /shares/<share>

    setfacl -Rm default:group:"<domain>\\Domain Admins":rwx /shares/<share>
    setfacl -Rm default:group:"<domain>\\Domain Users":r-x /shares/<share>

    smbcontrol all reload-config
    ```
- log files
  - mount process log file: `/var/log/samba_dm/mount`

# Sources
- https://wiki.samba.org/index.php/Setting_up_Samba_as_a_Domain_Member
- https://wiki.samba.org/index.php/Setting_up_a_Share_Using_POSIX_ACLs
- https://wiki.samba.org/index.php/Setting_up_a_Share_Using_Windows_ACLs
- https://wiki.samba.org/index.php/Troubleshooting_Samba_Domain_Members#The_net_Command_Fails_to_Connect_to_the_127.0.0.1_IP_Address
- https://scriptthe.net/2015/02/05/autofs-in-docker-containers/
- https://www.rsyslog.com/doc/v8-stable/configuration/modules/omprog.html
- https://www.samba.org/samba/docs/current/man-html/vfs_full_audit.8.html
- https://lists.samba.org/archive/samba/2017-March/207489.html
