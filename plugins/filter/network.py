import re
from ansible.module_utils.common.text.converters import to_native

class FilterModule(object):
    def filters(self):
        return {
            'mac_to_name': self.mac_to_name,
            'split_interfaces': self.split_interfaces,
            'ipv4_to_name': self.ipv4_to_name,
            'ipv6_to_name': self.ipv6_to_name
        }

    def mac_to_name(self, arg_sDevice, arg_dFacts, arg_bMacVlan):
        if re.match(r"^([0-9a-f]{2}:){5}[0-9a-f]{2}$", arg_sDevice):
            for key in arg_dFacts:
                if isinstance(arg_dFacts[key], dict) and key != 'default_ipv4' and 'macaddress' in arg_dFacts[key] and arg_dFacts[key]['macaddress'] == arg_sDevice:
                    return key
            # no interface found for mac, assume macvlan
            # maximum name length: 14 (https://stackoverflow.com/questions/24932172/what-length-can-a-network-interface-name-have)
            if arg_bMacVlan:
                return 'mv' + arg_sDevice.replace(':', '')
            else:
                raise Exception('No interface found with mac %s, creating macvlan interface disabled' % to_native(arg_sDevice))
        return arg_sDevice

    def ip_to_name(self, arg_sIP, arg_dFacts, arg_sIPVersion):
        for key in arg_dFacts:
            if isinstance(arg_dFacts[key], dict) and arg_sIPVersion in arg_dFacts[key]:
                if isinstance(arg_dFacts[key][arg_sIPVersion], dict):
                    if arg_dFacts[key][arg_sIPVersion]['address'] == arg_sIP:
                        return key
                else:
                    for ip in arg_dFacts[key][arg_sIPVersion]:
                        if ip['address'] == arg_sIP:
                            return key
        raise Exception('No interface found with ip %s' % to_native(arg_sIP))

    def ipv4_to_name(self, arg_sIPv4, arg_dFacts):
        return self.ip_to_name(arg_sIPv4, arg_dFacts, 'ipv4')

    def ipv6_to_name(self, arg_sIPv6, arg_dFacts):
        return self.ip_to_name(arg_sIPv6, arg_dFacts, 'ipv6')

    def split_interfaces(self, arg_sInterfacesFileContent):
        dInterfaces = {}
        sLastIface = ""
        for sLine in arg_sInterfacesFileContent.splitlines():
            if re.match(r"^(#|source).*", sLine): pass
            elif re.match(r"^\s+.*", sLine) and sLastIface:
                dInterfaces[sLastIface] += '\n' + sLine
            else:
                aStrs = re.split(r'\s+', sLine)
                if len(aStrs) > 1:
                    if aStrs[1] in dInterfaces: dInterfaces[aStrs[1]] += '\n'
                    else: dInterfaces[aStrs[1]] = ''
                    dInterfaces[aStrs[1]] += sLine
                    sLastIface = aStrs[1]
        return dInterfaces
