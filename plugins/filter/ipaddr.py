import ipaddress

class FilterModule(object):
    def filters(self):
        return {
            'netaddress': self.netaddress,
            'netmask': self.netmask,
            'netaddress_with_prefixlen': self.netaddress_with_prefixlen
        }

    def netaddress(self, arg_sNetwork):
        network = ipaddress.ip_network(arg_sNetwork, strict=False)
        return format(network.network_address)

    def netmask(self, arg_sNetwork):
        network = ipaddress.ip_network(arg_sNetwork, strict=False)
        return format(network.netmask)

    def netaddress_with_prefixlen(self, arg_sNetwork):
        network = ipaddress.ip_network(arg_sNetwork, strict=False)
        return format(network.with_prefixlen)