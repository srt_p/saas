import re

class FilterModule(object):
    def filters(self):
        return {
            'dns_name': self.dns_name
        }

    def dns_name(self, arg_sHost, arg_dGroups, arg_sGroup, arg_sDomain, arg_sGroupNameReplace = ''):
        if not arg_sGroupNameReplace: arg_sGroupNameReplace = arg_sGroup
        index = arg_dGroups[arg_sGroup].index(arg_sHost) + 1
        return arg_sGroupNameReplace + (str(index) if len(arg_dGroups[arg_sGroup]) > 1 else '') + (('.' + arg_sDomain) if arg_sDomain else '')
