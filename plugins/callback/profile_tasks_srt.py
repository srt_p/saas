# Make coding more python3-ish
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
    callback: profile_tasks_srt
    type: aggregate
    short_description: adds time information to tasks
    description:
      - Ansible callback plugin for timing individual tasks and overall execution time
      - It also lists the top/bottom time consuming tasks in the summary (configurable)
    requirements:
      - whitelisting in configuration
    options:
      sort_order:
        description: Adjust the sorting output of summary tasks
        choices: ['descending', 'ascending', 'descending_group', 'ascending_group', 'none']
        default: 'none'
        env:
          - name: PROFILE_TASKS_SRT_SORT_ORDER
        ini:
          - section: callback_profile_tasks_srt
            key: sort_order
      depth:
        description: Adjust the depth of the output, not usable with sort_order descending or ascending
        choices: ['task', 'play']
        default: 'task'
        env:
          - name: PROFILE_TASKS_SRT_DEPTH
        ini:
          - section: callback_profile_tasks_srt
            key: depth
'''

import time
from ansible.module_utils.six.moves import reduce
from ansible.plugins.callback import CallbackBase

class CallbackModule(CallbackBase):
    """
    This callback module provides per-task timing, ongoing playbook elapsed time
    and ordered list of longest running tasks at end.
    """
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'aggregate'
    CALLBACK_NAME = 'profile_tasks_srt'
    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self):
        self.stats = {}
        # example
        # {
        #   '<play.uuid>': {
        #     'name': '<play.name>',
        #     'task.uuid': {
        #       'name': 'task.name',
        #       'time': tasktime,
        #     }, {...}
        #   }
        # }
        self.current_play = None
        self.current_task = None
        self.sort_order = 'none'
        self.depth = 'task'
        self.last_time = time.time()
        super(CallbackModule, self).__init__()

    def set_options(self, task_keys=None, var_options=None, direct=None):
        super(CallbackModule, self).set_options(task_keys=task_keys, var_options=var_options, direct=direct)
        self.sort_order = self.get_option('sort_order')
        self.depth = self.get_option('depth')

    def _timestamp_task(self):
        if self.current_play is not None:
            if self.current_task in self.stats[self.current_play]:
                # currently the 'time' entry contains the starting time
                self.stats[self.current_play][self.current_task]['time'] = time.time() - self.stats[self.current_play][self.current_task]['time']

    def _record_task(self, task):
        self._timestamp_task()

        # record the start time of the current task
        self.current_task = task._uuid
        if self.current_play is not None:
            self.stats[self.current_play][self.current_task] = {'time': time.time(), 'name': task.get_name()}
        #if self._display.verbosity >= 2:
        #    self.stats[self.current_play][self.current_task]['path'] = task.get_path()
    
    def _record_play(self, play):
        self._timestamp_task()
        self.current_play = play._uuid
        self.stats[self.current_play] = {'name': play.get_name()}

    def v2_playbook_on_task_start(self, task, is_conditional):
        self._record_task(task)

    def v2_playbook_on_handler_task_start(self, task):
        self._record_task(task)
    
    def v2_playbook_on_play_start(self, play):
        self._record_play(play)

    def _print_stat(self, name, time, fchar=" "):
        time_str = u'{0:.02f}'.format(time)
        fill_width = max(0, self._display.columns - len(name) - len(time_str) - 3)
        self._display.display('%s %s %ss ' % (name, fchar * fill_width, time_str))

    def playbook_on_stats(self, stats):
        self._timestamp_task()
        self.current_play = None
        self.current_task = None

        for play in self.stats:
            self.stats[play]['time'] = sum(self.stats[play][task]['time'] for task in self.stats[play] if (isinstance(self.stats[play][task], dict) and 'time' in self.stats[play][task]))
        self._display.display('total time: {0:.02f}s, sort order: {1}'.format(sum(self.stats[play]['time'] for play in self.stats), self.sort_order))
        if self.sort_order in ['descending', 'ascending']:
            tasks = {}
            for play in self.stats:
                for task in self.stats[play]:
                    if 'time' in self.stats[play][task]:
                        tasks[task] = self.stats[play][task]
                        tasks[task]['play'] = self.stats[play]['name']
            tasks_sort = {k: v for k, v in sorted(tasks.items(), reverse=(self.sort_order == 'descending'), key=lambda item: item[1]['time'])}
            for task in tasks_sort:
                self._print_stat(tasks_sort[task]['name'] + ' (' + tasks_sort[task]['play'] + ')', tasks_sort[task]['time'], fchar='-')
        elif self.sort_order in ['descending_group', 'ascending_group', 'none']:
            stats = self.stats
            if self.sort_order in ['descending_group', 'ascending_group']:
                stats = {k: v for k, v in sorted(stats.items(), reverse=(self.sort_order == 'descending_group'), key=lambda item: item[1]['time'])}
            for play in stats:
                self._print_stat(stats[play]['name'], stats[play]['time'], fchar='-')
                if self.depth == 'task':
                    for task in stats[play]:
                        if isinstance(stats[play][task], dict) and 'time' in stats[play][task]:
                            self._print_stat('  ' + stats[play][task]['name'], stats[play][task]['time'])
