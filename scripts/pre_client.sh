# https://unix.stackexchange.com/questions/6345/how-can-i-get-distribution-name-and-version-number-in-a-simple-shell-script

# install packages
packages='sudo python3 openssh'
packages_debian='sudo python3 openssh-server'
if [ -x "$(command -v apk)" ]; then
    sed -i -E "s=^(#)?http:=\1https:=g" /etc/apk/repositories
    sed -i -E "s=v\d+\.\d+/(\bmain\b|\bcommunity\b)=latest-stable/\1=g" /etc/apk/repositories
    sed -i -E "s=^#(http.*/community)$=\1=g" /etc/apk/repositories
    apk update
    apk upgrade
    apk add --no-cache $packages
    rc-update add sshd
    /etc/init.d/sshd start
elif [ -x "$(command -v pacman)" ]; then
    sed -i -e "s/^#Color$/Color/g" /etc/pacman.conf
    pacman -Syu --noconfirm
    pacman -S --noconfirm --needed $packages
    systemctl enable --now sshd
elif [ -x "$(command -v zypper)" ]; then
    zypper dist-upgrade -y
    zypper install -y $packages
    systemctl enable --now sshd
elif [ -x "$(command -v apt)" ]; then
    apt dist-upgrade -y
    apt install -y $packages_debian
    systemctl enable --now ssh
fi

echo ""
echo enter password for user root
while : ; do passwd root && break; done

# add user sysadmin
getent passwd sysadmin > /dev/null 2&>1
if [ $? -ne 0 ]; then
    if [ -x "$(command -v useradd)" ]; then
        groupadd sysadmin
        useradd -m -g sysadmin sysadmin
        echo ""
        echo enter password for user sysadmin
        while : ; do passwd sysadmin && break; done
    elif [ -x "$(command -v adduser)" ]; then
        adduser sysadmin
    fi
fi

cat > /etc/sudoers << EOL
Defaults secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
root ALL=(ALL) ALL
sysadmin ALL=(ALL:ALL) NOPASSWD:ALL
EOL

# ssh configuration
echo ""
while [ ! -f /home/sysadmin/.ssh/authorized_keys ]; do
    ips=$(ip -o addr list | grep -v ' lo ' | awk '{print $4}' | cut -d/ -f1)
    echo "first copy your public ssh key to /home/sysadmin/.ssh/authorized_keys, then press any key"
    echo "e.g. use ssh-copy-id sysadmin@<ip>"
    echo "possible ips:"
    echo "$ips"
    read  -n 1
done
chmod 700 /home/sysadmin/.ssh
chmod 600 /home/sysadmin/.ssh/authorized_keys
chown -R sysadmin:sysadmin /home/sysadmin/.ssh
cat > /etc/ssh/sshd_config << EOL
AllowUsers sysadmin
PermitRootLogin no
AuthorizedKeysFile .ssh/authorized_keys
PasswordAuthentication no
ChallengeResponseAuthentication no
PrintMotd no
EOL
if [ -x "$(command -v apt)" ]; then
cat >> /etc/ssh/sshd_config << EOL
Subsystem sftp /usr/lib/sftp-server
EOL
else
cat >> /etc/ssh/sshd_config << EOL
Subsystem sftp /usr/lib/ssh/sftp-server
EOL
fi
cat >> /etc/ssh/sshd_config << EOL
Match Address 192.168.0.0/16
    PasswordAuthentication yes
Match all
EOL

if [ -x "$(command -v systemctl)" ]; then
    if [ -e /lib/systemd/system/ssh.service ]; then
        systemctl stop ssh.service
        systemctl enable --now ssh.service
    else
        systemctl stop sshd.service
        systemctl enable --now sshd.service
    fi
elif [ -x "$(command -v adduser)" ]; then
    rc-service sshd stop
    rc-update add sshd
    rc-service sshd start
fi

echo ""
echo "please reboot"
echo ""
