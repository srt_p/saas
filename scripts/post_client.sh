#!/bin/sh

# remove user alarm
# e.g. added from archlinux-arm
getent passwd alarm > /dev/null 2&>1
if [ $? -eq 0 ]; then
    if [ -x "$(command -v userdel)" ]; then
        userdel -r alarm
        groupdel alarm
    elif [ -x "$(command -v deluser)" ]; then
        deluser --remove-home alarm
        delgroup alarm
    fi
fi

# remove user user
getent passwd user > /dev/null 2&>1
if [ $? -eq 0 ]; then
    if [ -x "$(command -v userdel)" ]; then
        userdel -r user
        groupdel user
    elif [ -x "$(command -v deluser)" ]; then
        deluser --remove-home user
        delgroup user
    fi
fi