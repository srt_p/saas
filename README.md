[[_TOC_]]

# Description
ansible setup for different services

# Preparation
## create certificate authority
on some pc do the following
- `openssl req -new -x509 -days 1825 -extensions v3_ca -keyout srt.key -out srt.crt`
  - supply password
  - examples config values:
    - Country Name: DE
    - State Name: Bayern
    - Org Name: SRT
    - Common Name: srt
- `cat srt.key | ansible-vault encrypt_string --stdin-name 'ca_key' > srt.key.enc`
- `ansible-vault encrypt_string --stdin-name 'ca_key_pw' > srt.keypw.enc`
- push the encrypted key, the encrypted passphrase and the certificate to source control under group_vars/managed.yml

## ansible client
- clone this directory\
`git clone https://gitlab.com/srt_p/saas`
- run `scripts/pre_client.sh`
- `reboot`
- login with newly created user `sysadmin`
- run `sudo scripts/post_client.sh`
- if the client is not yet registered in dhcp and has the wrong ip address, set a temporary one, e.g.\
`ip a add <x.x.x.x>/<x> broadcast + dev <interface>`

## ansible management host
- install `ansible` with package manager
- install ansible collections from ansible-galaxy:\
`ansible-galaxy collection install community.general`\
`ansible-galaxy collection install community.crypto`
- if starting new, generate ssh key **without passphrase**, e.g.\
`ssh-keygen -t ed25519`
- move ssh private key to `.ssh/id_...`
- set permissions for ssh\
`chmod 700 .ssh`\
`chmod 600 .ssh/id_...`
- clone this directory\
`git clone https://gitlab.com/srt_p/saas`

# Execution
## ansible run
- `ansible-playbook -t <tags> -i inventory/<inventory> playbooks/<playbook>`
- **run `update` before other playbooks**

## storage setup
- for adding new partitions to the storage clusters, manual configuration is required
- start with running the storage playbook with tag `prepare`, this installs the necessary packages
- create the filesystem (see https://docs.oracle.com/en/operating-systems/oracle-linux/6/admin/ol_create_ocfs2.html for more information)\
  `mkfs.ocfs2 -C <cluster size> -N 32 --fs-feature-level=max-features /dev/<disk>`
  | file system size | suggested minimum cluster size |
  | :---: | :---: |
  | 1 GB - 10 GB | 8K |
  | 10 GB - 100 GB | 16K |
  | 100 GB - 1 TB | 32K |
  | 1 TB - 10 TB | 64K |
  | 10 TB - 16 TB | 128K |

# Documentation
- [Certificate Stuff](misc/certificates.md)
- [Disable SSLv3](https://disablessl3.com/)

# Useful Link aggregation
- [tmux cheat sheet](https://gist.github.com/MohamedAlaa/2961058)
- [Dockerfile analyzer](https://www.fromlatest.io)

# Sources
- https://superuser.com/questions/906846/what-is-the-significance-of-an-ansible-task-reporting-that-something-has-changed/907602
