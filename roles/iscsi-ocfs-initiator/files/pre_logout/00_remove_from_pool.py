#!/usr/bin/env python3

import subprocess
import sys
import os

iscsi_id = sys.argv[1].split(':', 1)[1]
cluster = iscsi_id.split('.')[0]
sources = [f"/mnt/iscsi/{iscsi_id}/{entry.name}" for entry in os.scandir(f"/mnt/iscsi/{iscsi_id}") if entry.is_dir()]

try:
    p = subprocess.run(
        ['/usr/bin/xattr', '-p', 'user.mergerfs.branches', f"/mergerfs/{cluster}/.mergerfs"],
        capture_output=True, text=True
    )
    current_branches = [entry.split('=')[0] for entry in p.stdout.split(':')]
    if set(current_branches) - set(sources):
        subprocess.run(
            ['/usr/bin/umount', f"/mergerfs/{cluster}"],
            capture_output=True, check=True
        )
    else:
        subprocess.run(
            ['/usr/bin/xattr', '-w', 'user.mergerfs.branches', f"-{':'.join(sources)}", f"/mergerfs/{cluster}/.mergerfs"],
            capture_output=True, check=True
        )
except:
    sys.exit(1)