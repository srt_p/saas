#!/usr/bin/env python3

import sys
import os
import subprocess

iscsi_id = sys.argv[1].split(':', 1)[1]
cluster = iscsi_id.split('.')[0]
sources = [f"/mnt/iscsi/{iscsi_id}/{entry.name}" for entry in os.scandir(f"/mnt/iscsi/{iscsi_id}") if entry.is_dir()]

if os.path.exists(f"/mergerfs/{cluster}"):
    try:
        subprocess.run(['/usr/bin/systemd-mount', '-t', 'fuse.mergerfs', '-o', 'defaults', sources[0], f"/mergerfs/{cluster}"], capture_output=True, check=True)
        sources = sources[1:]
    except:
        sys.exit(1)

try:
    subprocess.run(['/usr/bin/xattr', '-w', 'user.mergerfs.branches', f"+>{':'.join(sources)}", f"/mergerfs/{cluster}/.mergerfs"], capture_output=True, check=True)
except:
    sys.exit(1)