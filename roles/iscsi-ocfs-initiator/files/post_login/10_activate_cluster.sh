#!/bin/bash

cluster=$(sed -n -E 's/^.*cluster\s?=\s?//p' /etc/ocfs2/cluster.conf)
if ! o2cb cluster-status "$cluster" | grep -q "Online"; then
    o2cb register-cluster "$cluster"
fi