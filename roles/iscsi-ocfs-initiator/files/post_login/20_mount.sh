#!/bin/bash

# Check if an argument is provided
if [ $# -eq 0 ]; then
    echo "Usage: $0 <target_name>"
    exit 1
fi

for target_file in /sys/class/iscsi_session/session*/targetname; do
    if [ -f "$target_file" ] && [ "$(cat "$target_file")" = "$1" ]; then
        session_dir=$(dirname "$target_file")
        for block_device in "${session_dir}"/device/target*/*/block/*; do
            if [ -b "/dev/$(basename "$block_device")" ]; then
                mkdir -p "/mnt/iscsi/${1#*:}/$(basename "$block_device")"
                if mount "/dev/$(basename "$block_device")" "/mnt/iscsi/${1#*:}/$(basename "$block_device")"; then
                    echo "Successfully mounted /dev/$(basename "$block_device") to /mnt/iscsi/${1#*:}/$(basename "$block_device")"
                else
                    echo "Failed to mount /dev/$(basename "$block_device") to /mnt/iscsi/${1#*:}/$(basename "$block_device")"
                    exit 1
                fi
            fi
        done
        break
    fi
done
