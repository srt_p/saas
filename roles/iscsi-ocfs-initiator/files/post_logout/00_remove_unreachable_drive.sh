#!/bin/bash

# Check if an argument is provided
if [ $# -eq 0 ]; then
    echo "Usage: $0 <target_name>"
    exit 1
fi

for target_file in /sys/class/iscsi_session/session*/targetname; do
    if [ -f "$target_file" ] && [ "$(cat "$target_file")" = "$1" ]; then
        session_dir=$(dirname "$target_file")
        for block_device in "${session_dir}"/device/target*/*/block/*; do
            echo 1 > "/sys/block/$(basename $block_device)/device/delete"
        done
        break
    fi
done
